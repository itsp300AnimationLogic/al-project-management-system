# AL Project Management System #

## What is this Project For? ##

This was a group project in college for Software Development Project 3 [ITSP300]

Students were required to form groups of five or six, look for a client and develop a desktop application for them based on their requirements

## Group Members and Contact Information ##

| Name                   | Email                      |
| ---------------------- | -------------------------- |
| Shaylen Reddy [Leader] | shaylenreddy42@gmail.com   |
| Shane Linden           | shanelinden1995@gmail.com  |
| Sahir Maharaj          | sahirmaharaj30@gmail.com   |
| Ayanda Mlangeni        | ayandamlangeni71@gmail.com |
| Eynar Roshev           | eynaroshev@gmail.com       |
| Johannes Smit          | hardussmit12@gmail.com     |

## A Little About the Project ##

The AL Project Management System was created to allow our client to effectively manage their projects and day-to-day tasks

It was written in Java and C++

### Features ###

* Typical register, login and logout system
* Staff sessions are recorded
* Reset password via a secret code
* A mailing system to notify staff members if
    * They have been assigned to a new project
    * Tasks were created for a project they have been assigned to
    * There's a new message in the chat
    * There's a new comment on a task
    * They have been [un]assigned from a task
    * A task has been marked as [in]complete
    * They request to reset their password, a secret code is sent

#### Administrator Features ####

* Create, update and delete projects
* Assign stuff members to newly created projects
* Can view the progress of each project
* View the database [all important tables including staff sessions]

#### Staff Features ####

* Update their account
* Create, update and delete tasks for projects their assigned to
* Real-time chat for projects and comments for tasks
* Assign and unassign members from tasks
* Mark tasks as [in]complete

## Third-party APIs and Libraries Used ##

* Absolute Layout from NetBeans
* [JavaMail API](https://javaee.github.io/javamail/)
* [MySQL JDBC Driver](https://dev.mysql.com/downloads/connector/j/5.1.html)

## How Do I Get Set Up? ##

* [JDK 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Visual Studio 2019 or 2017 Community Edition](https://visualstudio.microsoft.com/vs/)
* [CMake 3.8 or later](https://cmake.org/) [3.15 or later for the vs 2019 generator] and must be in your PATH

## Build Instructions ##

Run the `build project.cmd` script from the project root directory and the resulting installer will be located in the `installer` directory

The script begins by checking if Visual Studio 2019 or 2017 is installed. If not, then the process is terminated. If it does exist, the generator for CMake is set and CMake is run. The solution will be created in the `cmake build` directory and is built with MSBuild. After building, the files needed for packaging in the installer is copied from its locations to the `dist` directory and then the installer is created with Inno Setup 6 using the `alpms installer.iss` script and is located in the `installer` directory on completion
