@ECHO OFF
@ECHO.

IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat" (
	@ECHO Found Visual Studio 2019
	@ECHO.
	@CALL "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat"
	@ECHO.
	SET GENERATOR=Visual Studio 16
) ELSE IF EXIST "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat" ( 
	@ECHO Found Visual Studio 2017
	@ECHO.
	@CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat"
	@ECHO.
	SET GENERATOR=Visual Studio 15 Win64
) ELSE (
	@ECHO Visual Studio 2019 nor 2017 could be found
	@GOTO END
)

@MD "%~dp0cmake build"

@ECHO Running CMake
@ECHO.

cmake -G "%GENERATOR%" -S "%~dp0\" -B "%~dp0cmake build"

@ECHO.

IF EXIST "%~dp0cmake build\ALProjectManagementSystem.sln" ( 
	@ECHO Building solution with MSBuild
	@ECHO.
	
	MSBuild /property:Configuration="Release" "%~dp0cmake build\ALProjectManagementSystem.sln"

	@ECHO.
)

IF EXIST "%~dp0cmake build\Release\ALPMS Launcher.exe" ( 
	@COPY /Y "%~dp0cmake build\Release\ALPMS Launcher.exe" "%~dp0dist" /V
	@ECHO.
)

@XCOPY "%~dp0src/lib" "%~dp0dist/lib" /I /Y
@ECHO.

:INSTALLER

IF EXIST "%~dp0dist/ALPMS Launcher.exe" (
	@ECHO Creating Installer
	@ECHO.

	"%~dp0third party/Inno Setup 6/ISCC.exe" "%~dp0alpms installer.iss"

	@ECHO.
	@ECHO Installer Created Successfully
	@ECHO.
)

:END

PAUSE