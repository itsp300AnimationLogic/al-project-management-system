package alprojectmanagementsystem;

/**
 * <h1>Staff Sign Up Screen</h1>
 * <p>Staff members who wish to sign up
 * immediately when the system starts
 * up will be redirected from the login
 * screen to create their account here</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CreateAccount
{
    
    public CreateAccount()
    {
        //Creates new frame
        JFrame f = new JFrame("AL Project Management System");
        //f.setIconImage(new ImageIcon("").getImage()); //Sets the icon for frame
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 720); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new Login();
            }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("SIGN UP"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for first name
        JLabel lblFirstName = new JLabel("Name:");
        lblFirstName.setSize(lblFirstName.getPreferredSize());
        
        //Textfield for first name to be inputted
        JTextField txfFirstName = new JTextField(20);
        txfFirstName.setSize(txfFirstName.getPreferredSize());
        
        //Label for last name
        JLabel lblLastName = new JLabel("Surname:");
        lblLastName.setSize(lblLastName.getPreferredSize());
        
        //Textfield for last name to be inputted
        JTextField txfLastName = new JTextField(20);
        txfLastName.setSize(txfLastName.getPreferredSize());
        
        //Label for contact number
        JLabel lblNumber = new JLabel("Contact Number:");
        lblNumber.setSize(lblNumber.getPreferredSize());
        
        //Textfield for contact number to be inputted
        JTextField txfNumber = new JTextField(20);
        txfNumber.setSize(txfNumber.getPreferredSize());
        
        //Label for email address
        JLabel lblEmail = new JLabel("Email Address");
        lblEmail.setSize(lblEmail.getPreferredSize());
        
        //Textfield for email address to be inputted
        JFormattedTextField ftxfEmail = new JFormattedTextField();
        ftxfEmail.setSize(txfFirstName.getPreferredSize());
        
        //Label for password
        JLabel lblPassword = new JLabel("Enter New Password:");
        lblPassword.setSize(lblPassword.getPreferredSize());
        
        //Password field for a new password to be inputted
        JPasswordField pwfPassword = new JPasswordField(20);
        pwfPassword.setSize(pwfPassword.getPreferredSize());
        
        //Label for confirm password
        JLabel lblConfirm = new JLabel("Confirm New Password:");
        lblConfirm.setSize(lblConfirm.getPreferredSize());
        
        //Password field for a new password to be confirmed
        JPasswordField pwfConfirm = new JPasswordField(20);
        pwfConfirm.setSize(pwfConfirm.getPreferredSize());
        
        //Button to sign up a new staff member
        JButton btnSignUp = new JButton("Sign Up");
        btnSignUp.setSize(160, btnSignUp.getPreferredSize().height);
        btnSignUp.setBackground(Color.WHITE);
        btnSignUp.setForeground(Color.BLACK);
        btnSignUp.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String firstName = txfFirstName.getText(),
                       lastName  = txfLastName.getText(),
                       number    = txfNumber.getText(),
                       email     = ftxfEmail.getText(),
                       password  = "",
                       confirm   = "";
                
                char passwordArr[] = pwfPassword.getPassword(),
                     confirmArr[]  = pwfConfirm.getPassword();
                
                //Iterating through the char arrays to represent the passwords as Strings
                for (char c : passwordArr) password += c;
                for (char c : confirmArr) confirm += c;
                
                Common c1 = new Common();
                
                if (c1.staff(firstName, lastName, number, email, password, confirm, 0, Common.STAFFINSERT) == 0)
                {
                    f.dispose();
                    new UserDashboard();
                }
            }
        });
        
        //Button to cancel operation
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setSize(160, btnCancel.getPreferredSize().height);
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new Login();
            }
        });
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 80);
        lblFirstName.setLocation(((f.getWidth()-txfFirstName.getWidth())/2), 180);
        txfFirstName.setLocation(((f.getWidth()-txfFirstName.getWidth())/2), 200);
        lblLastName.setLocation(((f.getWidth()-txfLastName.getWidth())/2), 240);
        txfLastName.setLocation(((f.getWidth()-txfLastName.getWidth())/2), 260);
        lblNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2), 300);
        txfNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2), 320);
        lblEmail.setLocation(((f.getWidth()-ftxfEmail.getWidth())/2), 360);
        ftxfEmail.setLocation(((f.getWidth()-ftxfEmail.getWidth())/2), 380);
        lblPassword.setLocation(((f.getWidth()-pwfPassword.getWidth())/2), 420);
        pwfPassword.setLocation(((f.getWidth()-pwfPassword.getWidth())/2), 440);
        lblConfirm.setLocation(((f.getWidth()-pwfConfirm.getWidth())/2), 480);
        pwfConfirm.setLocation(((f.getWidth()-pwfConfirm.getWidth())/2), 500);
        btnSignUp.setLocation(((f.getWidth()-btnSignUp.getWidth())/2), 560);
        btnCancel.setLocation(((f.getWidth()-btnCancel.getWidth())/2), 600);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblFirstName);
        f.getContentPane().add(txfFirstName);
        f.getContentPane().add(lblLastName);
        f.getContentPane().add(txfLastName);
        f.getContentPane().add(lblNumber);
        f.getContentPane().add(txfNumber);
        f.getContentPane().add(lblEmail);
        f.getContentPane().add(ftxfEmail);
        f.getContentPane().add(lblPassword);
        f.getContentPane().add(pwfPassword);
        f.getContentPane().add(lblConfirm);
        f.getContentPane().add(pwfConfirm);
        f.getContentPane().add(btnSignUp);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
        
    }
    
}
