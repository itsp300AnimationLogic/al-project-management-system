package alprojectmanagementsystem;

import java.io.Serializable;

/**
 *
 * @author Eynar Roshev [eynaroshev@gmail.com]
 */
public class UserDetails implements Serializable {

    private String name;
    private String surname;
    private String contactNo;
    private String email;
    private String dateOfBirth;
    private String gender;
    private String address;

    public UserDetails(String name, String contactNo, String surname, String email, String dateOfBirth, String gender, String address) {
        this.name = name;
        this.surname = surname;
        this.contactNo = contactNo;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
