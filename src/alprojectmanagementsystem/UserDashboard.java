package alprojectmanagementsystem;

/**
 * <h1>User Dashboard Screen</h1>
 * <p>This is where users will be directed to on login.
 * It'll allow a user to view the projects they have
 * and tasks that are assigned to them</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class UserDashboard
{
    
    JFrame f;

    JLabel lblTitle;

    JScrollPane spForTblProjects,
                spForTblTasks;
    
    DefaultTableModel dtmProjects,
                      dtmTasks;

    JTable tblProjects,
           tblTasks;

    JButton btnViewProject,
            btnViewTask,
            btnUpdateAccount,
            btnLogout;
    
    Common cmn = new Common();

    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;

    public UserDashboard()
    {
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(800, 580);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                if (cmn.logout(true) == 0)
                {
                    f.dispose();
                }
            }
        });

        lblTitle = new JLabel("YOUR DASHBOARD");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());

        setTables();
        
        tblProjects = new JTable(dtmProjects);
        tblProjects.setForeground(Color.black);
        tblProjects.setBackground(Color.white);
        tblProjects.setFillsViewportHeight(true);
        tblProjects.setRowHeight(25);
        tblProjects.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        spForTblProjects = new JScrollPane(tblProjects);
        spForTblProjects.setSize(300, 300);
        
        tblTasks = new JTable(dtmTasks);
        tblTasks.setForeground(Color.black);
        tblTasks.setBackground(Color.white);
        tblTasks.setFillsViewportHeight(true);
        tblTasks.setRowHeight(25);
        tblTasks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        spForTblTasks = new JScrollPane(tblTasks);
        spForTblTasks.setSize(300, 300);

        btnViewProject = new JButton("View Project");
        btnViewProject.setSize(spForTblProjects.getWidth(), btnViewProject.getPreferredSize().height);
        btnViewProject.setForeground(Color.black);
        btnViewProject.setBackground(Color.white);
        btnViewProject.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    int selectedProject = tblProjects.getSelectedRow(),
                        projectID;
                    
                    String projectName;
                    
                    if (selectedProject > -1)
                    {
                        projectName = (String) tblProjects.getValueAt(selectedProject, 0);
                    
                        try
                        {
                            ps = dbConnection.prepareStatement("SELECT PROJECT_ID FROM PROJECTS WHERE PROJECT_NAME = ?");
                            ps.setString(1, projectName);

                            rs = ps.executeQuery();
                            
                            projectID = rs.next() ? rs.getInt(1) : -1;
                            
                            f.dispose();
                            new ViewProject(projectID, "User");
                        }
                        catch (SQLException se) {}
                    }
                }
            }
        );

        btnViewTask = new JButton("View Task");
        btnViewTask.setSize(spForTblTasks.getWidth(), btnViewTask.getPreferredSize().height);
        btnViewTask.setForeground(Color.black);
        btnViewTask.setBackground(Color.white);
        btnViewTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    int selectedTask = tblTasks.getSelectedRow(),
                        taskID;
                    
                    String taskName;

                    if (selectedTask > -1)
                    {
                        taskName = (String) tblTasks.getValueAt(selectedTask, 0);
                        
                        try
                        {
                            ps = dbConnection.prepareStatement
                            (
                                "SELECT TASK_ID FROM TASKS " +
                                "WHERE TASK_NAME = ?"
                            );
                            ps.setString(1, taskName);

                            rs = ps.executeQuery();

                            taskID = rs.next() ? rs.getInt(1) : -1;

                            rs.close();
                            ps.close();
                            
                            f.dispose();
                            new ViewTask(taskID, -1, "User");
                        }
                        catch (SQLException se) { JOptionPane.showMessageDialog(null, "Error", se.getMessage(), JOptionPane.ERROR_MESSAGE); }
                    }
                }
            }
        );
        
        btnUpdateAccount = new JButton("Update Account");
        btnUpdateAccount.setSize(btnUpdateAccount.getPreferredSize());
        btnUpdateAccount.setForeground(Color.black);
        btnUpdateAccount.setBackground(Color.white);
        btnUpdateAccount.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new UpdateAccount();
                }
            }
        );

        btnLogout = new JButton("Logout");
        btnLogout.setSize(btnLogout.getPreferredSize());
        btnLogout.setForeground(Color.black);
        btnLogout.setBackground(Color.white);
        btnLogout.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (cmn.logout(false) == 0)
                    {
                        f.dispose();
                    }
                }
            }
        );

        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 40);
        spForTblProjects.setLocation(((f.getWidth() - spForTblProjects.getWidth()) / 2) - spForTblProjects.getWidth() / 2 - 20, 120);
        spForTblTasks.setLocation(((f.getWidth() - spForTblTasks.getWidth()) / 2) + spForTblTasks.getWidth() / 2 + 20, 120);
        btnViewProject.setLocation(spForTblProjects.getX(), spForTblProjects.getY() + spForTblProjects.getHeight() + 10);
        btnViewTask.setLocation(spForTblTasks.getX(), spForTblTasks.getY() + spForTblTasks.getHeight() + 10);
        btnUpdateAccount.setLocation(20, 20);
        btnLogout.setLocation(f.getWidth() - btnLogout.getWidth() - 20, 20);

        f.getContentPane().add(lblTitle);
        f.getContentPane().add(spForTblProjects);
        f.getContentPane().add(spForTblTasks);
        f.getContentPane().add(btnViewProject);
        f.getContentPane().add(btnViewTask);
        f.getContentPane().add(btnUpdateAccount);
        f.getContentPane().add(btnLogout);

        f.setVisible(true);
        
        refreshTables();
    }

    void setTables()
    {
        Object tblProjectsHeaders[] = 
        {
            "Project Name",
            "Owner ?"
        };
        
        dtmProjects = new DefaultTableModel(tblProjectsHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        Object tblTasksHeaders[] = 
        {
            "Task Name",
            "Project Name",
            "Creator ?"
        };
        
        dtmTasks = new DefaultTableModel(tblTasksHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT PROJECT_ID FROM PROJECT_MEMBERS " + 
                "WHERE STAFF_ID = ?"
            );
            ps.setObject(1, (Object) cmn.getStaffID());

            rs = ps.executeQuery();

            while (rs.next())
            {
                int projectID = rs.getInt(1);

                PreparedStatement newPS = dbConnection.prepareStatement
                (
                    "SELECT PROJECT_NAME, OWNER_ID " +
                    "FROM PROJECTS " +
                    "WHERE PROJECT_ID = ?"
                );
                newPS.setInt(1, projectID);

                ResultSet newRS = newPS.executeQuery();

                if (newRS.next())
                {
                    Object project[] = 
                    {
                        newRS.getObject(1),
                        newRS.getInt(2) == Integer.parseInt(cmn.getStaffID()) ? "Yes" : "No"
                    };

                    dtmProjects.addRow(project);
                }

                newRS.close();
                newPS.close();
            }

            rs.close();
            ps.close();
            
            ps = dbConnection.prepareStatement
            (
                "SELECT TASK_ID FROM TASK_ASSIGNEES " + 
                "WHERE ASSIGNEE_ID = ?"
            );
            ps.setObject(1, (Object) cmn.getStaffID());

            rs = ps.executeQuery();

            while (rs.next())
            {
                int taskID = rs.getInt(1);

                PreparedStatement newPS = dbConnection.prepareStatement
                (
                    "SELECT t.TASK_NAME, t.TASK_CREATOR, p.PROJECT_NAME " +
                    "FROM TASKS t, PROJECTS p " +
                    "WHERE TASK_ID = ? AND p.PROJECT_ID = t.PROJECT_ID"
                );
                newPS.setInt(1, taskID);

                ResultSet newRS = newPS.executeQuery();

                if (newRS.next())
                {
                    Object task[] = 
                    {
                        newRS.getObject(1),
                        newRS.getObject(3),
                        newRS.getInt(2) == Integer.parseInt(cmn.getStaffID()) ? "Yes" : "No"
                    };

                    dtmTasks.addRow(task);
                }

                newRS.close();
                newPS.close();
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    final void refreshTables()
    {
        new Timer
        (
            10000,
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent ae)
                {
                    setTables();
                    tblProjects.setModel(dtmProjects);
                    tblTasks.setModel(dtmTasks);
                }
            }
        ).start();
    }
    
}
