package alprojectmanagementsystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 *
 * @author Eynar Roshev [eynaroshev@gmail.com]
 */
public class Client {

    private ObjectOutputStream output;
    private ObjectInputStream input;
    private Socket client;
    private UserDetails userDetails;
    private String serverResponse;
    private String host = "localhost";

    public static void main(String[] args) {
        Client client = new Client();
        client.runClient();
    }
    
    public void runClient() {
        try {
            // Will be, e.g., either login or sign-up details
            // and the server will handle these requests accordingly
            client = new Socket(host, 12345);
            System.out.println("Connected to host " + host);
            output = new ObjectOutputStream(client.getOutputStream());
            output.writeObject(userDetails);
            output.flush();

            input = new ObjectInputStream(client.getInputStream());
            serverResponse = input.readObject().toString();

            System.out.println(serverResponse);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
