package alprojectmanagementsystem;

/**
 * <h1>Update Account Screen</h1>
 * <p>Staff members can update their 
 * details here</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;

public class UpdateAccount
{
    
    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public UpdateAccount()
    {
        //Creates new frame
        JFrame f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(480, 480);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new UserDashboard();
            }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("UPDATE ACCOUNT");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        //Label for first name
        JLabel lblFirstName = new JLabel("Name:");
        lblFirstName.setSize(lblFirstName.getPreferredSize());
        
        //Textfield for first name to be inputted
        JTextField txfFirstName = new JTextField(20);
        txfFirstName.setSize(txfFirstName.getPreferredSize());
        
        //Label for last name
        JLabel lblLastName = new JLabel("Surname:");
        lblLastName.setSize(lblLastName.getPreferredSize());
        
        //Textfield for last name to be inputted
        JTextField txfLastName = new JTextField(20);
        txfLastName.setSize(txfLastName.getPreferredSize());
        
        //Label for contact number
        JLabel lblNumber = new JLabel("Contact Number:");
        lblNumber.setSize(lblNumber.getPreferredSize());
        
        //Textfield for contact number to be inputted
        JTextField txfNumber = new JTextField(20);
        txfNumber.setSize(txfNumber.getPreferredSize());
        
        //Button to sign up a new staff member
        JButton btnUpdateAccount = new JButton("Update Account");
        btnUpdateAccount.setSize(160, btnUpdateAccount.getPreferredSize().height);
        btnUpdateAccount.setBackground(Color.WHITE);
        btnUpdateAccount.setForeground(Color.BLACK);
        btnUpdateAccount.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String firstName = txfFirstName.getText(),
                       lastName  = txfLastName.getText(),
                       number    = txfNumber.getText();
                
                Common cmn = new Common();
                
                int staffID = Integer.parseInt(cmn.getStaffID());
                
                if (cmn.staff(firstName, lastName, number, "email@alpms.com", "", "", staffID, Common.STAFFUPDATE) == 0)
                {
                    f.dispose();
                    new UserDashboard();
                }
            }
        });
        
        //Button to cancel operation
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setSize(160, btnCancel.getPreferredSize().height);
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new UserDashboard();
            }
        });
        
        try
        {
            Common cmn = new Common();
            
            int staffID = Integer.parseInt(cmn.getStaffID());
            
            ps = dbConnection.prepareStatement
            (
                "SELECT FIRST_NAME, LAST_NAME, CONTACT_NUMBER " + 
                "FROM STAFF " +
                "WHERE STAFF_ID = ?"
            );
            ps.setInt(1, staffID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                txfFirstName.setText(rs.getString(1));
                txfLastName.setText(rs.getString(2));
                txfNumber.setText(rs.getString(3));
            }
            
            rs.close();
            ps.close();
        }
        catch (Exception e) { JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 40);
        lblFirstName.setLocation(((f.getWidth()-txfFirstName.getWidth())/2), 140);
        txfFirstName.setLocation(((f.getWidth()-txfFirstName.getWidth())/2), 160);
        lblLastName.setLocation(((f.getWidth()-txfLastName.getWidth())/2), 200);
        txfLastName.setLocation(((f.getWidth()-txfLastName.getWidth())/2), 220);
        lblNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2), 260);
        txfNumber.setLocation(((f.getWidth()-txfNumber.getWidth())/2), 280);
        btnUpdateAccount.setLocation(((f.getWidth()-btnUpdateAccount.getWidth())/2), 340);
        btnCancel.setLocation(((f.getWidth()-btnCancel.getWidth())/2), 380);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblFirstName);
        f.getContentPane().add(txfFirstName);
        f.getContentPane().add(lblLastName);
        f.getContentPane().add(txfLastName);
        f.getContentPane().add(lblNumber);
        f.getContentPane().add(txfNumber);
        f.getContentPane().add(btnUpdateAccount);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
        
    }
    
}
