package alprojectmanagementsystem;

/**
 * <h1>Update Task Screen</h1>
 * <p>This will be used to update tasks</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;

public class UpdateTask
{
    
    JFrame f;
    
    JLabel lblTitle,
           lblTaskName,
           lblTaskDescription;
    
    JTextField txfTaskName;
    
    JTextArea txaTaskDescription;
    
    JScrollPane spForTxa;
    
    JButton btnUpdateTask,
            btnBack;
    
    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public UpdateTask(int    p_taskID,
                      int    p_projectID,
                      String p_source)
    {
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(480, 480);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new ViewTask(p_taskID, p_projectID, p_source);
            }
        });
        
        lblTitle = new JLabel("UPDATE TASK");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        lblTaskName = new JLabel("Task Name:");
        lblTaskName.setSize(lblTaskName.getPreferredSize());
        
        txfTaskName = new JTextField(30);
        txfTaskName.setSize(txfTaskName.getPreferredSize());
        
        lblTaskDescription = new JLabel("Enter a Description of the Task:");
        lblTaskDescription.setSize(lblTaskDescription.getPreferredSize());
        
        txaTaskDescription = new JTextArea();
        
        spForTxa = new JScrollPane(txaTaskDescription, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spForTxa.setSize(txfTaskName.getWidth(), 60);
        
        btnUpdateTask = new JButton("Update Task");
        btnUpdateTask.setSize(btnUpdateTask.getPreferredSize());
        btnUpdateTask.setForeground(Color.black);
        btnUpdateTask.setBackground(Color.white);
        btnUpdateTask.addMouseListener
        (new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String taskName        = txfTaskName.getText(),
                           taskDescription = txaTaskDescription.getText();
                    
                    Common cmn = new Common();
                    
                    if (cmn.task(taskName, taskDescription, -1, p_projectID, new ArrayList(), p_taskID, Common.TASKUPDATE) == 0)
                    {
                        f.dispose();
                        new ViewTask(p_taskID, p_projectID, p_source);
                    }
                    
                }
            }
        );

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT TASK_NAME, TASK_DESCRIPTION " + 
                "FROM TASKS " +
                "WHERE TASK_ID = ?"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                txfTaskName.setText(rs.getString(1));
                txaTaskDescription.setText(rs.getString(2));
            }
        }
        catch (Exception e) { JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new ViewTask(p_taskID, p_projectID, p_source);
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 60);
        lblTaskName.setLocation((f.getWidth() - txfTaskName.getWidth()) / 2, 160);
        txfTaskName.setLocation((f.getWidth() - txfTaskName.getWidth()) / 2, lblTaskName.getY() + lblTaskName.getHeight() + 5);
        lblTaskDescription.setLocation((f.getWidth() - spForTxa.getWidth()) / 2, txfTaskName.getY() + txfTaskName.getHeight() + 20);
        spForTxa.setLocation((f.getWidth() - spForTxa.getWidth()) / 2, lblTaskDescription.getY() + lblTaskDescription.getHeight() + 5);
        btnUpdateTask.setLocation((f.getWidth() - btnUpdateTask.getWidth()) / 2, spForTxa.getY() + spForTxa.getHeight() + 50);

        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblTaskName);
        f.getContentPane().add(txfTaskName);
        f.getContentPane().add(lblTaskDescription);
        f.getContentPane().add(spForTxa);
        f.getContentPane().add(btnUpdateTask);

        f.setVisible(true);
    }
    
}
