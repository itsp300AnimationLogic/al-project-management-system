package alprojectmanagementsystem;

/**
 * <h1>Create Task Screen</h1>
 * <p>This will be used to create new tasks</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class CreateTask
{
    
    JFrame f;
    
    JLabel lblTitle,
           lblTaskName,
           lblTaskDescription,
           lblSelectMembers;
    
    JTextField txfTaskName;
    
    JTextArea txaTaskDescription;
    
    JComboBox cbxSelectMembers;
    
    JScrollPane spForTxa,
                spForTbl;
    
    DefaultTableModel dtm;
    
    JTable tblMembers;
    
    JButton btnAddMember,
            btnCreateTask,
            btnBack;
    
    Connection c = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    Object tblMembersHeaders[] = 
    {
        "Staff ID",
        "First Name",
        "Last Name",
        "Email"
    };
    
    public CreateTask(int    p_staffID,
                      int    p_projectID,
                      String p_sourceForViewProject)
    {
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(480, 640);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new ViewProject(p_projectID, p_sourceForViewProject);
            }
        });
        
        lblTitle = new JLabel("CREATE TASK");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        lblTaskName = new JLabel("Task Name:");
        lblTaskName.setSize(lblTaskName.getPreferredSize());
        
        txfTaskName = new JTextField(30);
        txfTaskName.setSize(txfTaskName.getPreferredSize());
        
        lblTaskDescription = new JLabel("Enter a Description of the Task:");
        lblTaskDescription.setSize(lblTaskDescription.getPreferredSize());
        
        txaTaskDescription = new JTextArea();
        
        spForTxa = new JScrollPane(txaTaskDescription, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spForTxa.setSize(txfTaskName.getWidth(), 60);
        
        lblSelectMembers = new JLabel("Assign Members:");
        lblSelectMembers.setSize(lblSelectMembers.getPreferredSize());
        
        cbxSelectMembers = new JComboBox(fillComboBox(p_projectID));
        cbxSelectMembers.setSize(txfTaskName.getWidth() - 67, txfTaskName.getHeight());
        cbxSelectMembers.setBackground(Color.white);
        cbxSelectMembers.setSelectedIndex(0);
        
        btnAddMember = new JButton("Add");
        btnAddMember.setSize(btnAddMember.getPreferredSize().width, cbxSelectMembers.getHeight());
        btnAddMember.setForeground(Color.black);
        btnAddMember.setBackground(Color.white);
        btnAddMember.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String selectedMember = cbxSelectMembers.getSelectedItem().toString();
                    selectedMember = selectedMember.substring(0, selectedMember.indexOf(" - "));
                    int iSelectedMember = Integer.parseInt(selectedMember);

                    boolean found = false;
                    
                    for (int i = 0; i < dtm.getRowCount(); i++)
                    {
                        if ((int) dtm.getValueAt(i, 0) == iSelectedMember)
                        {
                            found = true;
                        }
                    }

                    if (!found)
                    {
                        try
                        {
                            ps = c.prepareStatement("SELECT * FROM STAFF WHERE STAFF_ID = ?");
                            ps.setInt(1, iSelectedMember);
                            rs = ps.executeQuery();

                            if (rs.next());
                            {
                                Object row[] = 
                                {
                                    rs.getObject(1),
                                    rs.getObject(2),
                                    rs.getObject(3),
                                    rs.getObject(5)
                                };

                                dtm.addRow(row);
                            }
                            
                            rs.close();
                            ps.close();
                        }
                        catch (SQLException ex) { System.out.println(ex.getMessage()); }
                    }
                }
            }
        );

        setDTM();
        
        tblMembers = new JTable(dtm);
        tblMembers.setForeground(Color.black);
        tblMembers.setBackground(Color.white);
        tblMembers.setFillsViewportHeight(true);
        tblMembers.setRowHeight(25);
        tblMembers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        spForTbl = new JScrollPane(tblMembers);
        spForTbl.setSize(txfTaskName.getWidth(), 120);
        
        btnCreateTask = new JButton("Create Task");
        btnCreateTask.setSize(btnCreateTask.getPreferredSize());
        btnCreateTask.setForeground(Color.black);
        btnCreateTask.setBackground(Color.white);
        btnCreateTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String taskName        = txfTaskName.getText(),
                           taskDescription = txaTaskDescription.getText();
                    
                    ArrayList taskAssignees = new ArrayList();
                    
                    for (int i = 0; i < tblMembers.getRowCount(); i++)
                    {
                        taskAssignees.add(tblMembers.getValueAt(i, 0));
                    }
                    
                    Common cmn = new Common();
                    
                    if (cmn.task(taskName, taskDescription, p_staffID, p_projectID, taskAssignees, -1, Common.TASKINSERT) == 0)
                    {
                        f.dispose();
                        new ViewProject(p_projectID, p_sourceForViewProject);
                    }
                    
                }
            }
        );

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new ViewProject(p_projectID, p_sourceForViewProject);
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 80);
        lblTaskName.setLocation((f.getWidth() - txfTaskName.getWidth()) / 2, 160);
        txfTaskName.setLocation((f.getWidth() - txfTaskName.getWidth()) / 2, lblTaskName.getY() + lblTaskName.getHeight() + 5);
        lblTaskDescription.setLocation((f.getWidth() - spForTxa.getWidth()) / 2, txfTaskName.getY() + txfTaskName.getHeight() + 20);
        spForTxa.setLocation((f.getWidth() - spForTxa.getWidth()) / 2, lblTaskDescription.getY() + lblTaskDescription.getHeight() + 5);
        lblSelectMembers.setLocation((f.getWidth() - txfTaskName.getWidth()) / 2, spForTxa.getY() + spForTxa.getHeight() + 20);
        cbxSelectMembers.setLocation((f.getWidth() - cbxSelectMembers.getWidth() - btnAddMember.getWidth() - 10) / 2, lblSelectMembers.getY() + lblSelectMembers.getHeight() + 5);
        btnAddMember.setLocation(cbxSelectMembers.getLocation().x + cbxSelectMembers.getWidth() + 10, cbxSelectMembers.getY());
        spForTbl.setLocation((f.getWidth() - spForTbl.getWidth()) / 2, cbxSelectMembers.getY() + cbxSelectMembers.getHeight() + 20);
        btnCreateTask.setLocation((f.getWidth() - btnCreateTask.getWidth()) / 2, spForTbl.getY() + spForTbl.getHeight() + 30);

        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblTaskName);
        f.getContentPane().add(txfTaskName);
        f.getContentPane().add(lblTaskDescription);
        f.getContentPane().add(spForTxa);
        f.getContentPane().add(lblSelectMembers);
        f.getContentPane().add(cbxSelectMembers);
        f.getContentPane().add(btnAddMember);
        f.getContentPane().add(spForTbl);
        f.getContentPane().add(btnCreateTask);

        f.setVisible(true);
    }
    
    private Vector<Object> fillComboBox(int p_projectID)
    {
        Vector members = new Vector();
        
        try
        {
            ps = c.prepareStatement
            (
                "SELECT STAFF_ID, EMAIL FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = ?)"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();
            
            while (rs.next())
            {
                members.add(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch(SQLException ex) { System.out.println(ex.getMessage()); }
        
        return members;
    }
    
    private void setDTM()
    {
        dtm = new DefaultTableModel(tblMembersHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
    }
    
}
