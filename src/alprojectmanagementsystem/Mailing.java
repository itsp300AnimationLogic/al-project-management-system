package alprojectmanagementsystem;

import java.sql.*;

/**
 * <h1>Mailing Class</h1>
 * This class is responsible for
 * mailing staff members updates
 * regarding the system
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.JOptionPane;

public class Mailing
{
    
    public Mailing() { setProperties(); }

    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    //Email account information
    private final String USERNAME = "itsp300.alpms",
                         PASSWORD = "alpms1234",
                         HOST     = "smtp.gmail.com";
    
    //Variables used to send emails
    private final Properties properties = System.getProperties();
    private final Session session = Session.getDefaultInstance(properties);
    private MimeMessage emailContent = new MimeMessage(session);
    private Transport transport = null;

    /**
     * This method adds the required properties
     * to the properties variable [properties] to allow
     * emails to be set
     */
    private void setProperties()
    {
        properties.put("mail.smtp.starttls.enable", "true"); //Enable Transport Layer Security
        properties.put("mail.smtp.host", HOST); //The service used to send emails [GMail]
        properties.put("mail.smtp.user", USERNAME); //Sets the username for the email account [movieshackstore]
        properties.put("mail.smtp.password", PASSWORD); //Sets the password to be used for the email account
        properties.put("mail.smtp.port", "587"); //Sets the official Simple Mail Transfer Protocol [SMTP] port number
        properties.put("mail.smtp.auth", "true"); //Enables authentication
    }
    
    /**
     * This method is used to set the MimeMessage
     * and the Transport for an email and then
     * sends it
     */
    private void sendEmail(ArrayList p_recipientEmails,
                           String    p_subject, 
                           String    p_body)
    {
        InternetAddress[] recipients = new InternetAddress[p_recipientEmails.size()];
        
        try
        {
            for (int i = 0; i < p_recipientEmails.size(); i++)
            {
                recipients[i] = new InternetAddress(p_recipientEmails.get(i).toString());
            }
            
            emailContent.setFrom(new InternetAddress(USERNAME, "AL Project Management System")); //itsp300.alpms@gmail.com
            emailContent.setRecipients(Message.RecipientType.TO, recipients);
            emailContent.setSubject(p_subject);
            emailContent.setContent(p_body, "text/html");
            
            transport = session.getTransport("smtp"); //Sets the transport to SMTP
            transport.connect(HOST, USERNAME, PASSWORD); //Connect GMail with the credentials of the store
            transport.sendMessage(emailContent, emailContent.getAllRecipients()); //Sends the email
            transport.close(); //Closes the transport
            
            emailContent = new MimeMessage(session); //Refreshes emailContent to avoid accumulating recipients
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    void emailNewProjectCreated(int p_projectID)
    {
        ArrayList recipientEmails = new ArrayList();
        
        String subject = "ALPMS: You Have Been Assigned to a New Project",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<table border='1' cellpadding='10' style='width: 100%; border-collapse: collapse'>";
        body += "<thead>";
        body += "<tr>";
        body += "<th colspan='6' style='text-align: center'>Project Details</th>";
        body += "</tr>";
        body += "<tr>";
        body += "<th>ID</th>";
        body += "<th>Name</th>";
        body += "<th>Description</th>";
        body += "<th>Start Date</th>";
        body += "<th>End Date</th>";
        body += "<th>Owner</th>";
        body += "</tr>";
        body += "</thead>";
        body += "<tbody>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT p.*, s.FIRST_NAME, s.LAST_NAME " +
                "FROM PROJECTS p, STAFF s " + 
                "WHERE p.PROJECT_ID = ? AND s.STAFF_ID = p.OWNER_ID"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                body += "<tr>";
                body += "<td>" + rs.getObject(1) + "</td>";
                body += "<td>" + rs.getObject(2) + "</td>";
                body += "<td>" + rs.getObject(3) + "</td>";
                body += "<td>" + rs.getObject(4) + "</td>";
                body += "<td>" + rs.getObject(5) + "</td>";
                body += "<td>" + rs.getObject(7) + " " + rs.getObject(8) + "</td>";
                body += "</tr>";
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        body += "</tbody>";
        body += "</table>";
        body += "<hr style='margin-top: 25px; margin-bottom: 25px'>";
        body += "<table border='1' cellpadding='10' style='width: 100%; border-collapse: collapse'>";
        body += "<thead>";
        body += "<tr>";
        body += "<th colspan='5' style='text-align: center'>Project Members</th>";
        body += "</tr>";
        body += "<tr>";
        body += "<th>ID</th>";
        body += "<th>First Name</th>";
        body += "<th>Last Name</th>";
        body += "<th>Contact Number</th>";
        body += "<th>Email</th>";
        body += "</tr>";
        body += "</thead>";
        body += "<tbody>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT * FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = ?)" +
                "ORDER BY STAFF_ID ASC"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                body += "<tr>";
                body += "<td>" + rs.getObject(1) + "</td>";
                body += "<td>" + rs.getObject(2) + "</td>";
                body += "<td>" + rs.getObject(3) + "</td>";
                body += "<td>" + rs.getObject(4) + "</td>";
                body += "<td>" + rs.getObject(5) + "</td>";
                body += "</tr>";

                recipientEmails.add(rs.getString(5));
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        body += "</tbody>";
        body += "</table>";
        
        sendEmail(recipientEmails, subject, body);
    }
    
    void emailNewTaskCreated(int p_taskID)
    {
        ArrayList recipientEmails = new ArrayList();
        
        String subject = "ALPMS: A New Task Has Been Created",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<table border='1' cellpadding='10' style='width: 100%; border-collapse: collapse'>";
        body += "<thead>";
        body += "<tr>";
        body += "<th colspan='5' style='text-align: center'>Task Details</th>";
        body += "</tr>";
        body += "<tr>";
        body += "<th>ID</th>";
        body += "<th>Name</th>";
        body += "<th>Description</th>";
        body += "<th>Creator</th>";
        body += "<th>Project</th>";
        body += "</tr>";
        body += "</thead>";
        body += "<tbody>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.*, s.FIRST_NAME, s.LAST_NAME, p.PROJECT_NAME " +
                "FROM TASKS t, PROJECTS p, STAFF s " + 
                "WHERE t.TASK_ID = ? AND s.STAFF_ID = t.TASK_CREATOR AND p.PROJECT_ID = t.PROJECT_ID"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                body += "<tr>";
                body += "<td>" + rs.getObject(1) + "</td>";
                body += "<td>" + rs.getObject(2) + "</td>";
                body += "<td>" + rs.getObject(3) + "</td>";
                body += "<td>" + rs.getObject(7) + " " + rs.getObject(8) + "</td>";
                body += "<td>" + rs.getObject(9) + "</td>";
                body += "</tr>";
            }
            
            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        body += "</tbody>";
        body += "</table>";
        body += "<hr style='margin-top: 25px; margin-bottom: 25px'>";
        body += "<table border='1' cellpadding='10' style='width: 100%; border-collapse: collapse'>";
        body += "<thead>";
        body += "<tr>";
        body += "<th colspan='5' style='text-align: center'>Assigned Members</th>";
        body += "</tr>";
        body += "<tr>";
        body += "<th>ID</th>";
        body += "<th>First Name</th>";
        body += "<th>Last Name</th>";
        body += "<th>Contact Number</th>";
        body += "<th>Email</th>";
        body += "</tr>";
        body += "</thead>";
        body += "<tbody>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT * FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT ASSIGNEE_ID FROM TASK_ASSIGNEES WHERE TASK_ID = ?)" +
                "ORDER BY STAFF_ID ASC"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                body += "<tr>";
                body += "<td>" + rs.getObject(1) + "</td>";
                body += "<td>" + rs.getObject(2) + "</td>";
                body += "<td>" + rs.getObject(3) + "</td>";
                body += "<td>" + rs.getObject(4) + "</td>";
                body += "<td>" + rs.getObject(5) + "</td>";
                body += "</tr>";
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        body += "</tbody>";
        body += "</table>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = " +
                "   (SELECT PROJECT_ID FROM TASKS WHERE TASK_ID = ?))" +
                "ORDER BY STAFF_ID ASC"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                recipientEmails.add(rs.getString(1));
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) {}
        
        sendEmail(recipientEmails, subject, body);
    }

    void emailNewMessageInChat(int p_messageID)
    {
        int projectID = -1;

        String message     = "",
               time        = "",
               projectName = "",
               fullName    = "";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT pc.*, p.PROJECT_NAME, s.FIRST_NAME, s.LAST_NAME " +
                "FROM PROJECT_CHATROOM pc, PROJECTS p, STAFF s " +
                "WHERE pc.MESSAGE_ID = ? AND s.STAFF_ID = pc.MESSENGER_ID " +
                "AND p.PROJECT_ID = pc.PROJECT_ID"
            );
            ps.setInt(1, p_messageID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                message = rs.getString(2);
                time = rs.getTimestamp(3).toString();
                time = time.substring(0, time.lastIndexOf("."));
                projectID = rs.getInt(5);
                projectName = rs.getString(6);
                fullName = rs.getString(7) + " " + rs.getString(8);
            }
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        ArrayList recipientEmails = new ArrayList();
        
        String subject = "ALPMS: You Have a New Message",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += fullName + " Sent a New Message in Project:<br>";
        body += "<span style='text-decoration: underline'>" + projectName + "</span>";
        body += "</h2>";
        body += "<div style='width: 100%; border: 1px solid black'>";
        body += "<table cellpadding='10' style='width: 100%'>";
        body += "<tbody>";
        body += "<tr>";
        body += "<td style='color: blue'>" + fullName + "</td>";
        body += "<td style='text-align: right'>" + time + "</td>";
        body += "</tr>";
        body += "<tr>";
        body += "<td colspan='2'>" + message + "</td>";
        body += "</tr>";
        body += "</tbody>";
        body += "</table>";
        body += "</div>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = ?) " +
                "ORDER BY STAFF_ID ASC"
            );
            ps.setInt(1, projectID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                recipientEmails.add(rs.getString(1));
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        sendEmail(recipientEmails, subject, body);
    }

    void emailNewCommentOnTask(int p_commentID)
    {
        int projectID = -1;

        String comment     = "",
               time        = "",
               taskName    = "",
               projectName = "",
               fullName    = "";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT tc.COMMENT_MSG, tc.COMMENT_DATE, t.TASK_NAME, " + 
                "p.PROJECT_ID, p.PROJECT_NAME, s.FIRST_NAME, s.LAST_NAME " +
                "FROM TASK_COMMENTS tc, TASKS t, PROJECTS p, STAFF s " +
                "WHERE tc.COMMENT_ID = ? AND t.TASK_ID = tc.TASK_ID AND " +
                "s.STAFF_ID = tc.COMMENTER_ID AND " +
                "p.PROJECT_ID = " + 
                "   (SELECT PROJECT_ID FROM TASKS WHERE TASK_ID = " +
                "       (SELECT TASK_ID FROM TASK_COMMENTS WHERE COMMENT_ID = ?))"
            );
            ps.setInt(1, p_commentID);
            ps.setInt(2, p_commentID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                comment = rs.getString(1);
                time = rs.getTimestamp(2).toString();
                time = time.substring(0, time.lastIndexOf("."));
                taskName = rs.getString(3);
                projectID = rs.getInt(4);
                projectName = rs.getString(5);
                fullName = rs.getString(6) + " " + rs.getString(7);
            }
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        ArrayList recipientEmails = new ArrayList();
        
        String subject = "ALPMS: New Comment Made on a Task",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += fullName + " Commented on<br>Task ";
        body += "<span style='text-decoration: underline'>" + taskName + "</span> in<br>";
        body += "Project <span style='text-decoration: underline'>" + projectName + "</span>";
        body += "</h2>";
        body += "<div style='width: 100%; border: 1px solid black'>";
        body += "<table cellpadding='10' style='width: 100%'>";
        body += "<tbody>";
        body += "<tr>";
        body += "<td style='color: blue'>" + fullName + "</td>";
        body += "<td style='text-align: right'>" + time + "</td>";
        body += "</tr>";
        body += "<tr>";
        body += "<td colspan='2'>" + comment + "</td>";
        body += "</tr>";
        body += "</tbody>";
        body += "</table>";
        body += "</div>";

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = ?) " +
                "ORDER BY STAFF_ID ASC"
            );
            ps.setInt(1, projectID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                recipientEmails.add(rs.getString(1));
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        sendEmail(recipientEmails, subject, body);
    }

    void emailAssignedToATask(String p_assignerName,
                              int    p_assignerID,
                              int    p_taskID,
                              int    p_staffID)
    {
        String taskName    = "",
               projectName = "";

        ArrayList recipientEmails = new ArrayList();
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.TASK_NAME, p.PROJECT_NAME " +
                "FROM TASKS t, PROJECTS p " +
                "WHERE t.TASK_ID = ? AND p.PROJECT_ID = t.PROJECT_ID"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                taskName = rs.getString(1);
                projectName = rs.getString(2);
            }

            rs.close();
            ps.close();
            
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL " +
                "FROM STAFF " +
                "WHERE STAFF_ID = ?"
            );
            ps.setInt(1, p_staffID);

            rs = ps.executeQuery();

            recipientEmails.add(rs.next() ? rs.getString(1) : "");

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        String subject = "ALPMS: You Have Been Assigned to a Task",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += p_assignerName + " Assigned You to <br>Task ";
        body += "<span style='text-decoration: underline'>" + taskName + "</span> in<br>";
        body += "Project <span style='text-decoration: underline'>" + projectName + "</span>";
        body += "</h2>";

        if (p_assignerID != p_staffID) sendEmail(recipientEmails, subject, body);
    }

    void emailTaskRemarked(String p_markerName,
                           int    p_taskID,
                           int    p_complete)
    {
        String taskName    = "",
               projectName = "";

        ArrayList recipientEmails = new ArrayList();
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.TASK_NAME, p.PROJECT_NAME " +
                "FROM TASKS t, PROJECTS p " +
                "WHERE t.TASK_ID = ? AND p.PROJECT_ID = t.PROJECT_ID"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                taskName = rs.getString(1);
                projectName = rs.getString(2);
            }

            rs.close();
            ps.close();
            
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL " +
                "FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "   (SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = " +
                "       (SELECT PROJECT_ID FROM TASKS WHERE TASK_ID = ?))"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                recipientEmails.add(rs.getString(1));
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        String subject = "ALPMS: A Task Has Been Remarked",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += p_markerName + " Marked <br>Task ";
        body += "<span style='text-decoration: underline'>" + taskName + "</span> in<br>";
        body += "Project <span style='text-decoration: underline'>" + projectName + "</span> ";
        body += "As " + (p_complete == 1 ? "Complete" : "Incomplete");
        body += "</h2>";

        sendEmail(recipientEmails, subject, body);
    }

    void emailUnassignedFromATask(String p_assignerName,
                                  int    p_assignerID,
                                  int    p_taskID,
                                  int    p_staffID)
    {
        String taskName    = "",
               projectName = "";

        ArrayList recipientEmails = new ArrayList();
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.TASK_NAME, p.PROJECT_NAME " +
                "FROM TASKS t, PROJECTS p " +
                "WHERE t.TASK_ID = ? AND p.PROJECT_ID = t.PROJECT_ID"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                taskName = rs.getString(1);
                projectName = rs.getString(2);
            }

            rs.close();
            ps.close();
            
            ps = dbConnection.prepareStatement
            (
                "SELECT EMAIL " +
                "FROM STAFF " +
                "WHERE STAFF_ID = ?"
            );
            ps.setInt(1, p_staffID);

            rs = ps.executeQuery();

            recipientEmails.add(rs.next() ? rs.getString(1) : "");

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        String subject = "ALPMS: You Have Been Unassigned from a Task",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += p_assignerName + " Unassigned You From <br>Task ";
        body += "<span style='text-decoration: underline'>" + taskName + "</span> in<br>";
        body += "Project <span style='text-decoration: underline'>" + projectName + "</span>";
        body += "</h2>";

        if (p_assignerID != p_staffID) sendEmail(recipientEmails, subject, body);
    }

    void emailSecretCode(String p_email,
                         String p_secretCode)
    {
        ArrayList recipientEmails = new ArrayList();
        recipientEmails.add(p_email);

        String subject = "ALPMS: Here is Your Secret Code",
               body    = "";

        body += "<h1 style='text-align: center; font-size: 24'>AL Project Management System</h1>";
        body += "<hr style='margin-bottom: 25px'>";
        body += "<h2 style='text-align: center; font-size: 18; margin-bottom: 25px'>";
        body += "You can change your password using this code<br>";
        body += "<span style='font-size: 20px'>" + p_secretCode + "</span>";
        body += "</h2>";

        sendEmail(recipientEmails, subject, body);
    }
    
}
