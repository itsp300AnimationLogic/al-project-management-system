package alprojectmanagementsystem;

/**
 * <h1>Task Screen</h1>
 * <p>Used to tasks details, 
 * mark it as complete,
 * assign and remove members
 * and leave comments</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ViewTask
{
    
    JFrame f;
    
    JLabel lblTitle;
    
    JPanel pnlAssignedMembers,
           pnlComments,
           pnlScrollableComments,
           pnlTaskDetails,
           pnlBlank;
    
    JScrollPane spForTblAssignees,
                spForComments,
                spForTxaComment;
    
    DefaultTableModel dtmAssignees;
    
    JTable tblAssignees;
    
    JTextArea txaComment;
    
    JComboBox cbxPotentialAssignees;
    
    JButton btnAddAssignee,
            btnRemoveAssignee,
            btnMakeComment,
            btnUpdateTask,
            btnDeleteTask,
            btnBack;

    JLabel lblTaskDetails[][];
    
    JCheckBox chkComplete;
    
    Common cmn = new Common();

    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;

    int commentCount = 0;
    
    public ViewTask(int    p_taskID,
                    int    p_projectID,
                    String p_source)
    {
        f = new JFrame("AL Task Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(800, 860);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                
                if (p_projectID > 0) new ViewProject(p_projectID, p_source);
                else if (p_source.equals("User")) new UserDashboard();
            }
        });
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT TASK_NAME FROM TASKS " +
                "WHERE TASK_ID = ?"
            );
            ps.setInt(1, p_taskID);
            
            rs = ps.executeQuery();
            
            lblTitle = new JLabel(rs.next() ? rs.getString(1).toUpperCase() : "Task");
            
            rs.close();
            ps.close();
        }
        catch (SQLException se) {}

        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        pnlAssignedMembers = new JPanel();
        pnlAssignedMembers.setLayout(null);
        pnlAssignedMembers.setBorder(BorderFactory.createTitledBorder("Assigned Members:"));
        pnlAssignedMembers.setSize(300, 440);
        pnlAssignedMembers.setBackground(Color.white);
        
        tblAssignees = new JTable();
        tblAssignees.setForeground(Color.black);
        tblAssignees.setBackground(Color.white);
        tblAssignees.setFillsViewportHeight(true);
        tblAssignees.setRowHeight(25);
        tblAssignees.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        setDTM(p_taskID);
        
        spForTblAssignees = new JScrollPane(tblAssignees);
        spForTblAssignees.setSize(pnlAssignedMembers.getWidth() - 20, pnlAssignedMembers.getHeight() - 140);

        cbxPotentialAssignees = new JComboBox(fillComboBox(p_taskID));
        cbxPotentialAssignees.setSize(pnlAssignedMembers.getWidth() - 20, cbxPotentialAssignees.getPreferredSize().height);
        cbxPotentialAssignees.setBackground(Color.white);
        
        btnAddAssignee = new JButton("Add Assignee");
        btnAddAssignee.setSize(spForTblAssignees.getWidth(), btnAddAssignee.getPreferredSize().height);
        btnAddAssignee.setForeground(Color.black);
        btnAddAssignee.setBackground(Color.white);
        btnAddAssignee.setEnabled(p_source.equals("User"));
        btnAddAssignee.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (p_source.equals("User"))
                    {
                        String staffMember;
                        int staffID;

                        if (cbxPotentialAssignees.getItemCount() > 0)
                        {
                            staffMember = (String) cbxPotentialAssignees.getSelectedItem();
                            staffMember = staffMember.substring(0, staffMember.indexOf(" - "));

                            staffID = Integer.parseInt(staffMember);

                            if (cmn.addTaskAssignee(staffID, p_taskID) == 0)
                            {
                                setDTM(p_taskID);
                                refreshCbxPotentialAssignees(p_taskID);
                            }
                        }
                    }
                }
            }
        );
        
        btnRemoveAssignee = new JButton("Remove Assignee");
        btnRemoveAssignee.setSize(spForTblAssignees.getWidth(), btnRemoveAssignee.getPreferredSize().height);
        btnRemoveAssignee.setForeground(Color.black);
        btnRemoveAssignee.setBackground(Color.white);
        btnRemoveAssignee.setEnabled(p_source.equals("User"));
        btnRemoveAssignee.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (p_source.equals("User"))
                    {
                        int selectedAssignee = tblAssignees.getSelectedRow(),
                            staffID;

                        if (selectedAssignee > -1)
                        {
                            staffID = (int) tblAssignees.getValueAt(selectedAssignee, 0);

                            if (cmn.removeTaskAssignee(staffID, p_taskID) == 0)
                            {
                                setDTM(p_taskID);
                                refreshCbxPotentialAssignees(p_taskID);
                            }
                        }
                    }
                }
            }
        );
        
        pnlComments = new JPanel();
        pnlComments.setLayout(null);
        pnlComments.setBorder(BorderFactory.createTitledBorder("Comments:"));
        pnlComments.setSize(300, 440);
        pnlComments.setBackground(Color.white);
        
        pnlScrollableComments = new JPanel();
        pnlScrollableComments.setLayout(new GridLayout(0, 1));
        pnlScrollableComments.setBackground(Color.white);
        
        spForComments = new JScrollPane();
        spForComments.setSize(pnlComments.getWidth() - 20, pnlComments.getHeight() - 80);
        spForComments.getViewport().add(pnlScrollableComments);
        spForComments.getViewport().setBackground(Color.white);
        
        btnMakeComment = new JButton("Comment");
        btnMakeComment.setSize(btnMakeComment.getPreferredSize().width, btnMakeComment.getPreferredSize().height + 10);
        btnMakeComment.setForeground(Color.black);
        btnMakeComment.setBackground(Color.white);
        btnMakeComment.setEnabled(p_source.equals("User"));
        btnMakeComment.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (p_source.equals("User"))
                    {
                        String comment = txaComment.getText();

                        if (cmn.makeCommentInTask(comment, p_taskID) == 0)
                        {
                            txaComment.setText("");
                            getCommentsForCurrentTask(p_taskID);
                        }
                    }
                }
            }
        );
        
        btnUpdateTask = new JButton("Update Task");
        btnUpdateTask.setSize(btnUpdateTask.getPreferredSize());
        btnUpdateTask.setForeground(Color.black);
        btnUpdateTask.setBackground(Color.white);
        btnUpdateTask.setVisible(p_source.equals("User"));
        btnUpdateTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new UpdateTask(p_taskID, p_projectID, p_source);
                }
            }
        );
        
        btnDeleteTask = new JButton("Delete Task");
        btnDeleteTask.setSize(btnUpdateTask.getPreferredSize());
        btnDeleteTask.setForeground(Color.black);
        btnDeleteTask.setBackground(Color.white);
        btnDeleteTask.setVisible(p_source.equals("User"));
        btnDeleteTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this task ?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION)
                    {
                        cmn.delete(p_taskID, Common.TASKDELETE);
                        f.dispose();
                        
                        if (p_projectID > 0) new ViewProject(p_projectID, p_source);
                        else if (p_source.equals("User")) new UserDashboard();
                    }
                    
                    
                }
            }
        );
        
        txaComment = new JTextArea();
        
        spForTxaComment = new JScrollPane(txaComment, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spForTxaComment.setSize(spForComments.getWidth() - btnMakeComment.getWidth() - 10, btnMakeComment.getHeight());

        pnlTaskDetails = new JPanel();
        pnlTaskDetails.setLayout(new GridLayout(7, 2));
        pnlTaskDetails.setBorder(BorderFactory.createTitledBorder("Task Details:"));
        pnlTaskDetails.setSize(pnlAssignedMembers.getWidth() * (5 / 2) + 40, 200);
        pnlTaskDetails.setBackground(Color.white);
        
        pnlBlank = new JPanel();
        pnlBlank.setBackground(Color.white);
        
        chkComplete = new JCheckBox();
        chkComplete.setBackground(Color.white);
        chkComplete.setEnabled(p_source.equals("User"));
        chkComplete.addActionListener
        (
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent ae)
                {
                    int complete = chkComplete.isSelected() ? 1 : 0;
                    
                    try
                    {
                        ps = dbConnection.prepareStatement
                        (
                            "UPDATE TASKS " +
                            "SET COMPLETE = ? " +
                            "WHERE TASK_ID = ?"
                        );
                        ps.setInt(1, complete);
                        ps.setInt(2, p_taskID);
                        
                        ps.executeUpdate();
                        ps.close();
                        
                        setTaskDetails(p_taskID);
                    }
                    catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

                    Mailing mail = new Mailing();
                    mail.emailTaskRemarked(cmn.getStaffFullName(), p_taskID, complete);
                }
            }
        );

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    
                    if (p_projectID > 0) new ViewProject(p_projectID, p_source);
                    else if (p_source.equals("User")) new UserDashboard();
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        btnUpdateTask.setLocation(f.getWidth() - btnUpdateTask.getWidth() - 20, 20);
        btnDeleteTask.setLocation(f.getWidth() - btnDeleteTask.getWidth() - 20, 60);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 40);
        pnlAssignedMembers.setLocation(((f.getWidth() - pnlAssignedMembers.getWidth()) / 2) - pnlAssignedMembers.getWidth() / 2 - 20, 120);
        pnlComments.setLocation(((f.getWidth() - pnlComments.getWidth()) / 2) + pnlComments.getWidth() / 2 + 20, 120);
        spForTblAssignees.setLocation((pnlAssignedMembers.getWidth() - spForTblAssignees.getWidth()) / 2, 20);
        cbxPotentialAssignees.setLocation((pnlAssignedMembers.getWidth() - spForTblAssignees.getWidth()) / 2, spForTblAssignees.getY() + spForTblAssignees.getHeight() + 10);
        btnAddAssignee.setLocation((pnlAssignedMembers.getWidth() - spForTblAssignees.getWidth()) / 2, cbxPotentialAssignees.getY() + cbxPotentialAssignees.getHeight() + 10);
        btnRemoveAssignee.setLocation((pnlAssignedMembers.getWidth() - spForTblAssignees.getWidth()) / 2, btnAddAssignee.getY() + btnAddAssignee.getHeight() + 10);
        spForComments.setLocation((pnlComments.getWidth() - spForComments.getWidth()) / 2, 20);
        spForTxaComment.setLocation(spForComments.getX(), spForComments.getY() + spForComments.getHeight() + 10);
        btnMakeComment.setLocation(spForTxaComment.getX() + spForTxaComment.getWidth() + 10, spForTxaComment.getY());
        pnlTaskDetails.setLocation((f.getWidth() - pnlTaskDetails.getWidth()) / 2, pnlAssignedMembers.getY() + pnlAssignedMembers.getHeight() + 20);
        
        f.getContentPane().add(btnBack);
        f.getContentPane().add(btnUpdateTask);
        f.getContentPane().add(btnDeleteTask);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(pnlAssignedMembers);
        f.getContentPane().add(pnlComments);
        pnlAssignedMembers.add(spForTblAssignees);
        pnlAssignedMembers.add(cbxPotentialAssignees);
        pnlAssignedMembers.add(btnAddAssignee);
        pnlAssignedMembers.add(btnRemoveAssignee);
        pnlComments.add(pnlScrollableComments);
        pnlComments.add(spForTxaComment);
        pnlComments.add(btnMakeComment);
        f.getContentPane().add(pnlTaskDetails);

        setTaskDetails(p_taskID);

        pnlTaskDetails.add(lblTaskDetails[0][0]);
        pnlTaskDetails.add(lblTaskDetails[0][1]);
        pnlTaskDetails.add(lblTaskDetails[1][0]);
        pnlTaskDetails.add(lblTaskDetails[1][1]);
        pnlTaskDetails.add(lblTaskDetails[2][0]);
        pnlTaskDetails.add(lblTaskDetails[2][1]);
        pnlTaskDetails.add(lblTaskDetails[3][0]);
        pnlTaskDetails.add(lblTaskDetails[3][1]);
        pnlTaskDetails.add(lblTaskDetails[4][0]);
        pnlTaskDetails.add(lblTaskDetails[4][1]);
        pnlTaskDetails.add(lblTaskDetails[5][0]);
        pnlTaskDetails.add(lblTaskDetails[5][1]);
        pnlTaskDetails.add(pnlBlank);
        pnlTaskDetails.add(chkComplete);
        
        getCommentsForCurrentTask(p_taskID);
        refreshComments(p_taskID);

        repaintComments();

        f.setVisible(true);
    }

    void setDTM(int p_taskID)
    {
        Object tblAssigneesHeaders[] = 
        {
            "Member ID",
            "First Name",
            "Last Name",
            "Email"
        };
        
        dtmAssignees = new DefaultTableModel(tblAssigneesHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT STAFF_ID, FIRST_NAME, LAST_NAME, EMAIL " + 
                "FROM STAFF " + 
                "WHERE STAFF_ID IN " +
                "(SELECT ASSIGNEE_ID FROM TASK_ASSIGNEES WHERE TASK_ID = ?)"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                Object assignee[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4)
                };

                dtmAssignees.addRow(assignee);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        tblAssignees.setModel(dtmAssignees);
    }
    
    Vector<Object> fillComboBox(int p_taskID)
    {
        Vector members = new Vector();
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT STAFF_ID, EMAIL FROM STAFF " +
                "WHERE STAFF_ID IN " +
                "(SELECT STAFF_ID FROM PROJECT_MEMBERS WHERE PROJECT_ID = " + 
                    "(SELECT PROJECT_ID FROM TASKS WHERE TASK_ID = ?)) " +
                "AND STAFF_ID NOT IN " +
                "(SELECT ASSIGNEE_ID FROM TASK_ASSIGNEES WHERE TASK_ID = ?)"
            );
            ps.setInt(1, p_taskID);
            ps.setInt(2, p_taskID);

            rs = ps.executeQuery();
            
            while (rs.next())
            {
                members.add(rs.getObject(1) + " - " + rs.getObject(2));
            }
            
            rs.close();
            ps.close();
        }
        catch(SQLException ex) { System.out.println(ex.getMessage()); }
        
        return members;
    }

    void setTaskDetails(int p_taskID)
    {
        pnlTaskDetails.removeAll();
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.*, s.FIRST_NAME, s.LAST_NAME, p.PROJECT_NAME " + 
                "FROM TASKS t, STAFF s, PROJECTS p " +
                "WHERE TASK_ID = ? AND s.STAFF_ID = p.OWNER_ID " +
                "AND p.PROJECT_ID = t.PROJECT_ID"
            );
            ps.setInt(1, p_taskID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                lblTaskDetails = new JLabel[6][2];
                
                lblTaskDetails[0][0] = new JLabel("ID:");
                lblTaskDetails[1][0] = new JLabel("Name:");
                lblTaskDetails[2][0] = new JLabel("Description:");
                lblTaskDetails[3][0] = new JLabel("Complete:");
                lblTaskDetails[4][0] = new JLabel("Creator:");
                lblTaskDetails[5][0] = new JLabel("Project:");

                for (int i = 0; i < lblTaskDetails.length; i++)
                {
                    lblTaskDetails[i][0].setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
                }
                
                lblTaskDetails[0][1] = new JLabel(rs.getObject(1).toString());
                lblTaskDetails[1][1] = new JLabel(rs.getObject(2).toString());
                lblTaskDetails[2][1] = new JLabel(rs.getObject(3).toString());
                lblTaskDetails[3][1] = new JLabel(rs.getInt(4) == 1 ? "Yes" : "No");
                lblTaskDetails[4][1] = new JLabel(rs.getObject(7) + " " + rs.getObject(8));
                lblTaskDetails[5][1] = new JLabel(rs.getObject(9).toString());
                
                chkComplete.setSelected(rs.getInt(4) == 1);
                chkComplete.setText(rs.getInt(4) == 1 ? "Mark as Incomplete" : "Mark as Complete");
            }
        }
        catch (SQLException se) {}

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                pnlTaskDetails.add(lblTaskDetails[i][j]);
            }
        }
        
        pnlTaskDetails.add(pnlBlank);
        pnlTaskDetails.add(chkComplete);
        
        pnlTaskDetails.repaint();
        pnlTaskDetails.revalidate();
    }
    
    void getCommentsForCurrentTask(int p_taskID)
    {
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT tc.*, s.FIRST_NAME, s.LAST_NAME " + 
                "FROM TASK_COMMENTS tc, STAFF s " +
                "WHERE tc.TASK_ID = ? AND s.STAFF_ID = tc.COMMENTER_ID " +
                "ORDER BY tc.COMMENT_ID ASC"
            );
            ps.setInt(1, p_taskID);
            
            rs = ps.executeQuery();
            
            // Get the row count
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            
            JLabel lblMembers[]  = new JLabel[rowCount],
                   lblTimes[]    = new JLabel[rowCount],
                   lblComments[] = new JLabel[rowCount];
            
            JPanel pnlForComments[]    = new JPanel[rowCount],
                   pnlForCommentInfo[] = new JPanel[rowCount];

            if (rowCount > commentCount)
            {
                commentCount = rowCount;
                pnlScrollableComments.removeAll();
                
                while (rs.next())
                {
                    int rowNumber = rs.getRow() - 1;

                    pnlForComments[rowNumber] = new JPanel();
                    pnlForComments[rowNumber].setLayout(new GridLayout(2, 1));
                    pnlForComments[rowNumber].setSize(spForComments.getWidth(), pnlForComments[rowNumber].getPreferredSize().height);
                    pnlForComments[rowNumber].setBackground(Color.white);
                    pnlForComments[rowNumber].setBorder(BorderFactory.createEtchedBorder());

                    pnlForCommentInfo[rowNumber] = new JPanel();
                    pnlForCommentInfo[rowNumber].setLayout(new GridLayout(1, 2));
                    pnlForCommentInfo[rowNumber].setBackground(Color.white);
                    
                    lblMembers[rowNumber] = new JLabel(rs.getString(6) + " " + rs.getString(7));
                    lblMembers[rowNumber].setSize(lblMembers[rowNumber].getPreferredSize());
                    lblMembers[rowNumber].setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
                    lblMembers[rowNumber].setForeground(Color.blue);
                    
                    lblTimes[rowNumber] = new JLabel(rs.getTimestamp(3).toString().substring(0, rs.getTimestamp(3).toString().length() - 2));
                    lblTimes[rowNumber].setSize(lblTimes[rowNumber].getPreferredSize());
                    lblTimes[rowNumber].setForeground(Color.black);
                    
                    lblComments[rowNumber] = new JLabel(rs.getString(2));
                    lblComments[rowNumber].setSize(lblComments[rowNumber].getPreferredSize());
                    lblComments[rowNumber].setForeground(Color.black);
                    lblComments[rowNumber].setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));
                    
                    pnlForCommentInfo[rowNumber].add(lblMembers[rowNumber]);
                    pnlForCommentInfo[rowNumber].add(lblTimes[rowNumber]);
                    
                    pnlForComments[rowNumber].add(pnlForCommentInfo[rowNumber]);
                    pnlForComments[rowNumber].add(lblComments[rowNumber]);

                    pnlForComments[rowNumber].setSize(pnlScrollableComments.getWidth(), pnlForComments[rowNumber].getMinimumSize().height);

                    pnlScrollableComments.add(pnlForComments[rowNumber]);
                }

                repaintComments();
            }
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }
    
    void refreshCbxPotentialAssignees(int p_taskID)
    {
        Point cbxLocation = cbxPotentialAssignees.getLocation();
        
        pnlAssignedMembers.remove(cbxPotentialAssignees);
        cbxPotentialAssignees = new JComboBox(fillComboBox(p_taskID));
        cbxPotentialAssignees.setSize(pnlAssignedMembers.getWidth() - 20, cbxPotentialAssignees.getPreferredSize().height);
        cbxPotentialAssignees.setBackground(Color.white);
        cbxPotentialAssignees.setLocation(cbxLocation);
        pnlAssignedMembers.add(cbxPotentialAssignees);
        
        pnlAssignedMembers.repaint();
        pnlAssignedMembers.revalidate();
    }

    final void refreshComments(int p_taskID)
    {
        new Timer
        (
            1000,
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent ae)
                {
                    getCommentsForCurrentTask(p_taskID);
                }
            }
        ).start();
    }

    void repaintComments()
    {
        pnlComments.remove(spForComments);
        spForComments = new JScrollPane();
        spForComments.setSize(pnlComments.getWidth() - 20, pnlComments.getHeight() - 80);
        spForComments.getViewport().add(pnlScrollableComments);
        spForComments.getViewport().setBackground(Color.white);
        spForComments.setLocation((pnlComments.getWidth() - spForComments.getWidth()) / 2, 20);
        pnlComments.add(spForComments);
        
        pnlScrollableComments.repaint();
        pnlScrollableComments.revalidate();
    }

}
