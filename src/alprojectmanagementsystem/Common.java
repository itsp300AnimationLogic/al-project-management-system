package alprojectmanagementsystem;

/**
 * <h1>Common Class</h1>
 * <p>This class is the backbone of
 * the project. It does all major
 * processes of the system</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

public class Common
{
    
    //Patterns for performing error handling on mostly staff and customers
    private final Pattern forNames               = Pattern.compile("\\A[A-Z]{2,20}\\z", Pattern.CASE_INSENSITIVE),
                          forDates               = Pattern.compile("\\A\\d{2}?/\\d{2}?/\\d{4}?\\z"),
                          forNumbers             = Pattern.compile("\\A\\d{10}\\z"),
                          forEmails              = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE),
                          forPasswords           = Pattern.compile("\\A.{8,}\\z"),
                          forProjectNames        = Pattern.compile("\\A[A-Z0-9 ]{4,20}\\z", Pattern.CASE_INSENSITIVE),
                          forProjectDescriptions = Pattern.compile("\\A[A-Z0-9\',./ ]{3,}\\z", Pattern.CASE_INSENSITIVE);

    //Initializing error messages to be used during error handling
    private final String initialErrMsg = "ERROR: SAMPLE [VALID RANGE]\r\n\r\n";
    private String finalErrMsg = null;
    
    //Initializing constants for checking operation type
    final static int STAFFDELETE   = -3,
                     STAFFUPDATE   = -2,
                     STAFFINSERT   = -1,
                     PROJECTINSERT =  1,
                     PROJECTUPDATE =  2,
                     PROJECTDELETE =  3,
                     TASKINSERT    =  4,
                     TASKUPDATE    =  5,
                     TASKDELETE    =  6;
    
    /**
     * Creating a calendar variable that is going to be used to get
     * the current date and be used to calculate the return date for movies
     * and also create the recommended formats for date and time
     */
    private final Calendar now = Calendar.getInstance();
    private final SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy"),
                                   sdfTime = new SimpleDateFormat("HH:mm:ss");
    
    //Variables to hold login details
    private static String currentUser,
                          currentUserID;
    private static Timestamp sessionStartTime;

    //Database constants
    private static final String DATABASE_URL      = "jdbc:mysql://localhost/itsp300_db?allowMultiQueries=true",
                                DATABASE_USER     = "root",
                                DATABASE_PASSWORD = "";
    
    //SQL variables used to interact with the database
    private static Connection dbConnection = null;
    private static PreparedStatement ps = null;
    private static ResultSet rs = null;

    Mailing mail = new Mailing();
    
    /**
     * Method checks if the database is connected
     * which will be used by the login screen
     * to determine database status
     */
    boolean isDatabaseConnected(Connection c) { return c != null; }
    
    //Returns a database connection
    static Connection getConnection()
    {
        try { dbConnection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD); }
        catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage()); }
        
        return dbConnection;
    }
    
    /**
     * Closes the database connection
     * which will be called when the
     * application is closed
     */
    void closeConnection()
    {
        if (dbConnection != null)
        {
            try { dbConnection.close(); }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
    }
    
    //Sets the login details to be passed to the store
    void setStaffLoginInformation(String staffFullName, String staffID, Timestamp startTime)
    {
        sessionStartTime = startTime;
        currentUser      = staffFullName;
        currentUserID    = staffID;
    }
    
    //Retrieves the login details
    String getStaffFullName() { return currentUser;   }
    String getStaffID()       { return currentUserID; }
    
    //Method for handling the login process
    int login(String email, String password)
    {
        finalErrMsg = initialErrMsg;
        int error = 0;
        
        if (forEmails.matcher(email).find() == false)
        {
            finalErrMsg += "Email: sample@email.com";
            error = 1;
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so, the error message will be shown
         * else a check will be made to see if it's the admin account
         * and if not, checks if the user is in the database
         * and only then an error message appears, telling the user
         * that the account does not exist if the check does not
         * form a result set with one row
         */
        if (error == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        else if (email.equalsIgnoreCase("admin@uncledonscaraudio.com") && password.equals("password"))
        {
            setStaffLoginInformation("Admin", "N/A", new Timestamp(now.getTimeInMillis()));
            new ManageProjectsHome().setVisible(true);
        }
        else
        {
            /**
             * Extracts the name, surname and staffID
             * from the database to be used to set
             * the staff login information
             */
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT FIRST_NAME, LAST_NAME, STAFF_ID " +
                    "FROM STAFF " +
                    "WHERE EMAIL = ? AND PASSWORD = ?"
                );
                ps.setString(1, email);
                ps.setString(2, password);
                
                rs = ps.executeQuery(); //Creates a result set
                
                if (rs.next())
                {
                    setStaffLoginInformation(rs.getObject(1) + " " + rs.getObject(2), rs.getObject(3) + "", new Timestamp(now.getTimeInMillis()));
                    new UserDashboard();
                }
                else error = 2;
                
                rs.close(); //Closes result set rs
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
            
        if (error == 2) JOptionPane.showMessageDialog(null, "Account does not exist", "Error", JOptionPane.ERROR_MESSAGE);
        
        return error;
    }
    
    //Method for handling the log out process
    int logout(boolean exit)
    {
        int dialogResult = -1; //Variable used to determine success
        String option = exit ? "exit ?" : "log out ?"; //Used for a more specific JOptionPane message
        
        /**
         * Shows the JOptionPane with the final message to the user
         * which asks them to confirm their action. If it is confirmed,
         * the diaglogResult variable changes to 0 [which means success]
         * and will then record the session into the database for
         * that specific user and reset login information.
         * The database connection is then closed and the action,
         * whether to log out or exit, determines what the system will do
         */
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to " + option, "Confirm", JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION)
        {
            dialogResult = JOptionPane.YES_OPTION;
            
            if (!getStaffFullName().equals("Admin"))
            {
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "INSERT INTO SESSIONS " +
                        "(START_TIME, END_TIME, STAFF_MEMBER_ID) " +
                        "VALUES (?, ?, ?)"
                    );
                    ps.setTimestamp(1, sessionStartTime);
                    ps.setTimestamp(2, new Timestamp(now.getTimeInMillis()));
                    ps.setInt(3, Integer.parseInt(currentUserID));

                    ps.executeUpdate(); //Executes prepared statement ps
                    ps.close(); //Closes prepared statement ps
                }
                catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
            }
            
            setStaffLoginInformation(null, null, null);
            
            if (exit) 
            {
                closeConnection();
                System.exit(0);
            }
            else new Login();
        }
        
        return dialogResult;
    }

    int sendSecretCode(String p_email)
    {
        finalErrMsg = initialErrMsg;
        int error = 0;

        if (forEmails.matcher(p_email).find() == false)
        {
            finalErrMsg += "Email: sample@email.com";
            error = 1;
        }
        
        if (error == 0)
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT SECRET_CODE FROM STAFF " +
                    "WHERE EMAIL = ?"
                );
                ps.setString(1, p_email);

                rs = ps.executeQuery();

                if (rs.next())
                {
                    mail.emailSecretCode(p_email, rs.getString(1));
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Account does not exist", "Error", JOptionPane.ERROR_MESSAGE);
                    error = 2;
                }
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }

        if (error == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Error", JOptionPane.ERROR_MESSAGE);

        return error;
    }

    //Method to reset a staff member's password
    int resetPassword(String p_email,
                      String p_secretCode,
                      String p_password,
                      String p_confirm)
    {
        finalErrMsg = initialErrMsg;
        int error = 0;
        
        Object errorChecking[][] =
        {
            { "Email: sample@email.com",                     forEmails.matcher(p_email).find()       },
            { "Password must be at least 8 characters long", forPasswords.matcher(p_password).find() },
            { "Passwords do not match!",                     p_confirm.equals(p_password)            }
        };
        
        /**
         * Looping through the errorChecking array
         * to find the checks that are false
         * in order to build the error message
         */
        for (int i = 0; i < errorChecking.length; i++)
        {
            if ((boolean) errorChecking[i][1] == false)
            {
                finalErrMsg += (String) errorChecking[i][0] + "\r\n";
                error = 1;
            }
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so then it will show the error message
         */
        if (error == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        else
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "SELECT SECRET_CODE FROM STAFF " +
                    "WHERE EMAIL = ?"
                );
                ps.setString(1, p_email);
                
                rs = ps.executeQuery();

                String secretCode = rs.next() ? rs.getString(1) : "";
                
                rs.close();
                ps.close();
                
                if (p_secretCode.equals(secretCode))
                {
                    ps = dbConnection.prepareStatement
                    (
                        "UPDATE STAFF " +
                        "SET " +
                        "   PASSWORD = ?, " +
                        "   SECRET_CODE = ? " +
                        "WHERE EMAIL = ?"
                    );
                    ps.setString(1, p_password);
                    ps.setString(2, generateSecretCode());
                    ps.setString(3, p_email);
                    
                    ps.executeUpdate();
                    ps.close();
                }
                else error = 2;
            }
            catch (SQLException ex) { JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        }
        
        if (error == 2) JOptionPane.showMessageDialog(null, "Secret Codes do not match !", "Error", JOptionPane.ERROR_MESSAGE);
        
        return error;
    }
    
    private int staffErrorHandling(String firstName,
                                   String lastName,
                                   String number,
                                   String email,
                                   String password,
                                   String confirm,
                                   int    checks)
    {
        //A 2D array containing {(String)Error: Sample [Valid Range], (boolean)The Check}
        Object errorChecking[][] =
        {
            { "Name: Name [2-20]",                           forNames.matcher(firstName).find()    },
            { "Surname: Surname [2-20]",                     forNames.matcher(lastName).find()     },
            { "Contact Number: 0123456789 [10]",             forNumbers.matcher(number).find()     },
            { "Email: example@email.com",                    forEmails.matcher(email).find()       },
            { "Password must be at least 8 characters long", forPasswords.matcher(password).find() },
            { "Passwords do not match!",                     confirm.equals(password)              }
        };
        
        //Initiating variables for error checking
        int errMsgChanged = 0;
        finalErrMsg = initialErrMsg;
        
        /**
         * Looping through the errorChecking array
         * to find the checks that are false
         * in order to build the error message
         */
        for (int i = 0; i < checks; i++)
        {
            if ((boolean) errorChecking[i][1] == false)
            {
                finalErrMsg += (String) errorChecking[i][0] + "\r\n";
                errMsgChanged = 1;
            }
        }
        
        /**
         * Checks if the error message changed through the variable
         * and if so then it will show the error message
         */
        if (errMsgChanged == 1) JOptionPane.showMessageDialog(null, finalErrMsg, "Invalid Input", JOptionPane.ERROR_MESSAGE);
        
        return errMsgChanged;
    }
    
    /**
     * Method used to determine if a date is valid
     * which is called in the relevant error handling method
     */
    private boolean isValidDate(String p_date)
    {
        /**
         * First checks if the date matches the pattern.
         * Only if it does, the method moves forward
         * to verify if the date is valid by parsing
         * it with sdfDate [a variable initialized earlier].
         * If the parsing fails and the error is caught,
         * the date is invalid and will set isValidDate
         * to false
         */
        
        boolean isValidDate = forDates.matcher(p_date).find();
        
        if (isValidDate)
        {
            try
            {
                sdfDate.setLenient(false);
                sdfDate.parse(p_date);
            }
            catch (ParseException pe) { isValidDate = false; }
        }
        
        return isValidDate;
    }

    private boolean compareDates(String p_startDate,
                                 String p_endDate)
    {
        boolean datesOkay = false;
        
        try
        {
            java.util.Date startDate = sdfDate.parse(p_startDate),
                           endDate   = sdfDate.parse(p_endDate);
            
            if (startDate.compareTo(endDate) < 0) datesOkay = true;
        }
        catch (ParseException pe) {}

        return datesOkay;
    }
    
    private boolean projectAndTaskErrorHandling(String p_name,
                                                String p_description,
                                                String p_startDate,
                                                String p_endDate,
                                                int    p_checks)
    {
        Object errorChecking[][] =
        {    
            { "Project Name: Name [4-20]\r\n",                       forProjectNames.matcher(p_name).find()               },
            { "Description: Can be set to \"N/A\" [min 3]\r\n",      forProjectDescriptions.matcher(p_description).find() },
            { "Start Date: dd/MM/yyyy\r\n",                          isValidDate(p_startDate)                             },
            { "End Date: dd/MM/yyyy\r\n",                            isValidDate(p_endDate)                               },
            { "End Date is equal to or earlier than Start Date\r\n", compareDates(p_startDate, p_endDate)                 }
        };
        
        boolean passed = true;
        finalErrMsg = initialErrMsg;

        for (int i = 0; i < p_checks; i++)
        {
            if ((boolean) errorChecking[i][1] == false)
            {
                finalErrMsg += (String) errorChecking[i][0];
                passed = false;
            }
        }

        if (passed == false) JOptionPane.showMessageDialog(null, finalErrMsg, "Error", JOptionPane.ERROR_MESSAGE);
        
        return passed;
    }

    String generateSecretCode()
    {
        Object characters[] = 
        {
              0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
            "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z"
        };

        String secretCode = "";

        for (int i = 0; i < 10; i++)
        {
            Random random = new Random();
            secretCode += characters[random.nextInt(characters.length)];
        }

        return secretCode;
    }
    
    //Method to insert or update staff
    int staff(String name,
              String surname,
              String number,
              String email,
              String password,
              String confirm,
              int    updateID,
              int    type)
    {
        int error = 0;
        
        //Ensures a neat format for these data ["Name" instead of "nAmE"]
        String sName    = name.length() > 0 ? name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase() : "",
               sSurname = surname.length() > 0 ? surname.substring(0, 1).toUpperCase() + surname.substring(1).toLowerCase() : "";
        
        //Checks the type to determine the route to take with the operation
        if (type == STAFFINSERT && staffErrorHandling(name, surname, number, email, password, confirm, 6) == 0)
        {
            //Inserts the new staff member into the database
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "INSERT INTO STAFF " +
                    "(FIRST_NAME, LAST_NAME, CONTACT_NUMBER, EMAIL, PASSWORD, SECRET_CODE) " +
                    "VALUES (?, ?, ?, ?, ?, ?)"
                );
                ps.setString(1, sName);
                ps.setString(2, sSurname);
                ps.setString(3, number);
                ps.setString(4, email);
                ps.setString(5, password);
                ps.setString(6, generateSecretCode());
                
                ps.executeUpdate(); //Executes prepared statement ps
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
            
            if (error == 0)
            {
                try
                {
                    ps = dbConnection.prepareStatement
                    (
                        "SELECT STAFF_ID FROM STAFF " +
                        "WHERE EMAIL = ?"
                    );
                    ps.setString(1, email);
                    
                    rs = ps.executeQuery();
                    
                    String staffID = rs.next() ? rs.getInt(1) + "" : "";
                    
                    setStaffLoginInformation(sName + " " + sSurname, staffID, new Timestamp(now.getTimeInMillis()));
                    
                    rs.close();
                    ps.close();
                }
                catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
            }
        }
        else if (type == STAFFUPDATE && staffErrorHandling(name, surname, number, email, password, confirm, 3) == 0)
        {
            //Updates staff member in the database
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "UPDATE STAFF " +
                    "SET " + 
                    "   FIRST_NAME     = ?, " +
                    "   LAST_NAME      = ?, " +
                    "   CONTACT_NUMBER = ? "  +
                    "WHERE STAFF_ID = ?"
                );
                ps.setString(1, sName);
                ps.setString(2, sSurname);
                ps.setString(3, number);
                ps.setInt(4, updateID);
                
                ps.executeUpdate(); //Executes prepared statement ps
                ps.close(); //Closes prepared statement ps
            }
            catch (SQLException ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else error = 1;
        
        return error;
    }
    
    int project(String    p_projectName,
                String    p_projectDescription,
                String    p_startDate,
                String    p_endDate,
                int       p_projectOwner,
                ArrayList p_projectMembers,
                int       p_projectID,
                int       p_type)
    {
        int error     = 0,
            projectID = -1;
        
        if (p_type == PROJECTINSERT && projectAndTaskErrorHandling(p_projectName, p_projectDescription, p_startDate, p_endDate, 5))
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "INSERT INTO PROJECTS (PROJECT_NAME, PROJECT_DESCRIPTION, START_DATE, END_DATE, OWNER_ID) " +
                    "VALUES (?, ?, ?, ?, ?)"
                );
                ps.setString(1, p_projectName);
                ps.setString(2, p_projectDescription);
                ps.setDate(3, new java.sql.Date(sdfDate.parse(p_startDate).getTime()));
                ps.setDate(4, new java.sql.Date(sdfDate.parse(p_endDate).getTime()));
                ps.setInt(5, p_projectOwner);

                ps.executeUpdate();
                ps.close();

                if (p_projectMembers.size() > 0)
                {
                    ps = dbConnection.prepareStatement
                    (
                        "SELECT PROJECT_ID FROM PROJECTS " +
                        "WHERE PROJECT_NAME = ?"
                    );
                    ps.setString(1, p_projectName);

                    rs = ps.executeQuery();

                    projectID = rs.next() ? rs.getInt(1) : -1;

                    rs.close();
                    ps.close();

                    for (Object memberID : p_projectMembers)
                    {
                        ps = dbConnection.prepareStatement
                        (
                            "INSERT INTO PROJECT_MEMBERS (STAFF_ID, PROJECT_ID) " +
                            "VALUES (?, ?)"
                        );
                        ps.setInt(1, (int) memberID);
                        ps.setInt(2, projectID);

                        ps.executeUpdate();
                        ps.close();
                    }
                }
            }
            catch (SQLException | ParseException e)
            { 
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else if (p_type == PROJECTUPDATE && projectAndTaskErrorHandling(p_projectName, p_projectDescription, p_startDate, p_endDate, 5))
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "UPDATE PROJECTS " +
                    "SET " +
                    "   PROJECT_NAME        = ?, " +
                    "   PROJECT_DESCRIPTION = ?, " +
                    "   START_DATE          = ?, " +
                    "   END_DATE            = ?, " +
                    "   OWNER_ID            = ? "  +
                    "WHERE PROJECT_ID = ?"
                );
                ps.setString(1, p_projectName);
                ps.setString(2, p_projectDescription);
                ps.setDate(3, new java.sql.Date(sdfDate.parse(p_startDate).getTime()));
                ps.setDate(4, new java.sql.Date(sdfDate.parse(p_endDate).getTime()));
                ps.setInt(5, p_projectOwner);
                ps.setInt(6, p_projectID);

                ps.executeUpdate();
                ps.close();
            }
            catch (SQLException | ParseException e)
            { 
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else error = 1;

        if (error == 0 && p_type == PROJECTINSERT) mail.emailNewProjectCreated(projectID);

        return error;
    }
    
    int task(String    p_taskName,
             String    p_taskDescription,
             int       p_taskCreator,
             int       p_projectID,
             ArrayList p_taskAssignees,
             int       p_taskID,
             int       p_type)
    {
        int error  =  0,
            taskID = -1;
        
        if (p_type == TASKINSERT && projectAndTaskErrorHandling(p_taskName, p_taskDescription, "-", "-", 2))
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "INSERT INTO TASKS (TASK_NAME, TASK_DESCRIPTION, TASK_CREATOR, PROJECT_ID) " +
                    "VALUES (?, ?, ?, ?)"
                );
                ps.setString(1, p_taskName);
                ps.setString(2, p_taskDescription);
                ps.setInt(3, p_taskCreator);
                ps.setInt(4, p_projectID);

                ps.executeUpdate();
                ps.close();

                ps = dbConnection.prepareStatement
                (
                    "SELECT TASK_ID FROM TASKS " +
                    "WHERE TASK_NAME = ?"
                );
                ps.setString(1, p_taskName);

                rs = ps.executeQuery();

                taskID = rs.next() ? rs.getInt(1) : -1;

                rs.close();
                ps.close();

                if (p_taskAssignees.size() > 0)
                {
                    for (Object assigneeID : p_taskAssignees)
                    {
                        ps = dbConnection.prepareStatement
                        (
                            "INSERT INTO TASK_ASSIGNEES (ASSIGNEE_ID, TASK_ID) " +
                            "VALUES (?, ?)"
                        );
                        ps.setInt(1, (int) assigneeID);
                        ps.setInt(2, taskID);

                        ps.executeUpdate();
                        ps.close();
                    }
                }
            }
            catch (SQLException e)
            { 
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else if (p_type == TASKUPDATE && projectAndTaskErrorHandling(p_taskName, p_taskDescription, "-", "-", 2))
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "UPDATE TASKS " +
                    "SET " +
                    "TASK_NAME        = ?, " +
                    "TASK_DESCRIPTION = ? "  +
                    "WHERE TASK_ID = ?"
                );
                ps.setString(1, p_taskName);
                ps.setString(2, p_taskDescription);
                ps.setInt(3, p_taskID);

                ps.executeUpdate();
                ps.close();
            }
            catch (SQLException e)
            { 
                JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else error = 1;

        if (error == 0 && p_type == TASKINSERT) mail.emailNewTaskCreated(taskID);
        
        return error;
    }
    
    int sendMessageInChat(String p_message,
                          int    p_projectID)
    {
        int error     =  0,
            messageID = -1;

        Timestamp messageDate = new Timestamp(now.getTimeInMillis());
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "INSERT INTO PROJECT_CHATROOM (MESSAGE, MESSAGE_DATE, MESSENGER_ID, PROJECT_ID) " +
                "VALUES (?, ?, ?, ?)"
            );
            ps.setString(1, p_message);
            ps.setTimestamp(2, messageDate);
            ps.setInt(3, Integer.parseInt(getStaffID()));
            ps.setInt(4, p_projectID);
            
            ps.executeUpdate();
            ps.close();

            ps = dbConnection.prepareStatement
            (
                "SELECT MESSAGE_ID FROM PROJECT_CHATROOM " +
                "WHERE " +
                "   MESSAGE      = ? AND " + 
                "   MESSAGE_DATE = ? AND " +
                "   MESSENGER_ID = ? AND " + 
                "   PROJECT_ID   = ?"
            );
            ps.setString(1, p_message);
            ps.setTimestamp(2, messageDate);
            ps.setInt(3, Integer.parseInt(getStaffID()));
            ps.setInt(4, p_projectID);

            rs = ps.executeQuery();
            
            messageID = rs.next() ? rs.getInt(1) : -1;
            
            rs.close();
            ps.close();
        }
        catch (SQLException se)
        { 
            JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }

        if (error == 0) mail.emailNewMessageInChat(messageID);
        
        return error;
    }
    
    int makeCommentInTask(String p_comment,
                          int    p_taskID)
    {
        int error     =  0,
            commentID = -1;

        Timestamp commentDate = new Timestamp(now.getTimeInMillis());
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "INSERT INTO TASK_COMMENTS (COMMENT_MSG, COMMENT_DATE, COMMENTER_ID, TASK_ID) " +
                "VALUES (?, ?, ?, ?)"
            );
            ps.setString(1, p_comment);
            ps.setTimestamp(2, commentDate);
            ps.setInt(3, Integer.parseInt(getStaffID()));
            ps.setInt(4, p_taskID);
            
            ps.executeUpdate();
            ps.close();

            ps = dbConnection.prepareStatement
            (
                "SELECT COMMENT_ID FROM TASK_COMMENTS " +
                "WHERE " +
                "   COMMENT_MSG  = ? AND " + 
                "   COMMENT_DATE = ? AND " +
                "   COMMENTER_ID = ? AND " + 
                "   TASK_ID      = ?"
            );
            ps.setString(1, p_comment);
            ps.setTimestamp(2, commentDate);
            ps.setInt(3, Integer.parseInt(getStaffID()));
            ps.setInt(4, p_taskID);

            rs = ps.executeQuery();
            
            commentID = rs.next() ? rs.getInt(1) : -1;
            
            rs.close();
            ps.close();
        }
        catch (SQLException se)
        { 
            JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }
        
        if (error == 0) mail.emailNewCommentOnTask(commentID);
        
        return error;
    }
    
    int addTaskAssignee(int p_staffID,
                        int p_taskID)
    {
        int error = 0;
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "INSERT INTO TASK_ASSIGNEES (ASSIGNEE_ID, TASK_ID) " +
                "VALUES (?, ?)"
            );
            ps.setInt(1, p_staffID);
            ps.setInt(2, p_taskID);
            
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException se)
        { 
            JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }

        if (error == 0) mail.emailAssignedToATask(getStaffFullName(), Integer.parseInt(getStaffID()), p_taskID, p_staffID);
        
        return error;
    }
    
    int removeTaskAssignee(int p_staffID,
                           int p_taskID)
    {
        int error = 0;
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "DELETE FROM TASK_ASSIGNEES " +
                "WHERE ASSIGNEE_ID = ? AND TASK_ID = ?"
            );
            ps.setInt(1, p_staffID);
            ps.setInt(2, p_taskID);
            
            ps.executeUpdate();
            ps.close();
        }
        catch (SQLException se)
        { 
            JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            error = 1;
        }

        if (error == 0) mail.emailUnassignedFromATask(getStaffFullName(), Integer.parseInt(getStaffID()), p_taskID, p_staffID);
        
        return error;
    }

    int delete(int p_id,
               int p_type)
    {
        int error = 0;
        
        if (p_type == PROJECTDELETE)
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "DELETE FROM TASK_COMMENTS WHERE TASK_ID IN " +
                    "   (SELECT TASK_ID FROM TASKS WHERE PROJECT_ID = ?); " +
                    "DELETE FROM TASK_ASSIGNEES WHERE TASK_ID IN " +
                    "   (SELECT TASK_ID FROM TASKS WHERE PROJECT_ID = ?); " +
                    "DELETE FROM TASKS WHERE PROJECT_ID = ?; " +
                    "DELETE FROM PROJECT_CHATROOM WHERE PROJECT_ID = ?; " +
                    "DELETE FROM PROJECT_MEMBERS WHERE PROJECT_ID = ?; " +
                    "DELETE FROM ACTIVITY_LOG WHERE PROJECT = ?; " +
                    "DELETE FROM PROJECTS WHERE PROJECT_ID = ?"

                );
                ps.setInt(1, p_id);
                ps.setInt(2, p_id);
                ps.setInt(3, p_id);
                ps.setInt(4, p_id);
                ps.setInt(5, p_id);
                ps.setInt(6, p_id);
                ps.setInt(7, p_id);
                
                ps.executeUpdate();
                ps.close();
            }
            catch (SQLException se)
            { 
                JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }
        else if (p_type == TASKDELETE)
        {
            try
            {
                ps = dbConnection.prepareStatement
                (
                    "DELETE FROM TASK_COMMENTS WHERE TASK_ID = ?; " +
                    "DELETE FROM TASK_ASSIGNEES WHERE TASK_ID = ?; " +
                    "DELETE FROM TASKS WHERE TASK_ID = ?"

                );
                ps.setInt(1, p_id);
                ps.setInt(2, p_id);
                ps.setInt(3, p_id);
                
                ps.executeUpdate();
                ps.close();
            }
            catch (SQLException se)
            { 
                JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                error = 1;
            }
        }

        return error;
    }

}
