DROP DATABASE IF EXISTS ITSP300_DB;
CREATE DATABASE ITSP300_DB;

USE ITSP300_DB;

SET SQL_MODE=ALLOW_INVALID_DATES;

DROP TABLE IF EXISTS TASK_COMMENTS;
DROP TABLE IF EXISTS TASK_ASSIGNEES;
DROP TABLE IF EXISTS TASKS;
DROP TABLE IF EXISTS ACTIVITY_LOG;
DROP TABLE IF EXISTS PROJECT_CHATROOM;
DROP TABLE IF EXISTS PROJECT_MEMBERS;
DROP TABLE IF EXISTS PROJECTS;
DROP TABLE IF EXISTS SESSIONS;
DROP TABLE IF EXISTS STAFF;

CREATE TABLE STAFF
(
	STAFF_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    FIRST_NAME VARCHAR(40) NOT NULL,
    LAST_NAME VARCHAR(40) NOT NULL,
    CONTACT_NUMBER VARCHAR(10) NOT NULL,
    EMAIL VARCHAR(100) NOT NULL,
    PASSWORD VARCHAR(50) NOT NULL,
    SECRET_CODE VARCHAR(20) NOT NULL
);

CREATE TABLE SESSIONS
(
	SESSION_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    START_TIME TIMESTAMP NOT NULL,
    END_TIME TIMESTAMP NOT NULL,
    STAFF_MEMBER_ID INT NOT NULL,
    FOREIGN KEY FK_STAFF_MEMBER_ID (STAFF_MEMBER_ID) REFERENCES STAFF (STAFF_ID)
);

CREATE TABLE PROJECTS
(
	PROJECT_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    PROJECT_NAME VARCHAR(40) NOT NULL UNIQUE,
    PROJECT_DESCRIPTION VARCHAR(2000),
    START_DATE DATE NOT NULL,
    END_DATE DATE NOT NULL,
    OWNER_ID INT NOT NULL,
    FOREIGN KEY FK_OWNER_ID (OWNER_ID) REFERENCES STAFF (STAFF_ID)
);

CREATE TABLE PROJECT_MEMBERS
(
	STAFF_ID INT NOT NULL,
    PROJECT_ID INT NOT NULL,
    FOREIGN KEY FK_STAFF_ID (STAFF_ID) REFERENCES STAFF (STAFF_ID),
    FOREIGN KEY FK_TM_PROJECT_ID (PROJECT_ID) REFERENCES PROJECTS (PROJECT_ID),
    UNIQUE KEY (STAFF_ID, PROJECT_ID)
);

CREATE TABLE PROJECT_CHATROOM
(
	MESSAGE_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    MESSAGE TEXT NOT NULL,
    MESSAGE_DATE TIMESTAMP NOT NULL,
    MESSENGER_ID INT NOT NULL,
    PROJECT_ID INT NOT NULL,
    FOREIGN KEY FK_MESSENGER_ID (MESSENGER_ID) REFERENCES STAFF (STAFF_ID),
    FOREIGN KEY FK_PC_PROJECT_ID (PROJECT_ID) REFERENCES PROJECTS (PROJECT_ID)
);

CREATE TABLE ACTIVITY_LOG
(
	ACTIVITY_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    ACTIVITY_DESCRIPTION TEXT NOT NULL,
    PROJECT INT NOT NULL,
    FOREIGN KEY FK_PROJECT (PROJECT) REFERENCES PROJECTS (PROJECT_ID)
);

CREATE TABLE TASKS
(
	TASK_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    TASK_NAME VARCHAR(40) NOT NULL UNIQUE,
    TASK_DESCRIPTION TEXT,
    COMPLETE INT NOT NULL DEFAULT 0,
    TASK_CREATOR INT NOT NULL,
    PROJECT_ID INT NOT NULL,
    FOREIGN KEY FK_TASK_CREATOR (TASK_CREATOR) REFERENCES STAFF (STAFF_ID),
    FOREIGN KEY FK_T_PROJECT_ID (PROJECT_ID) REFERENCES PROJECTS (PROJECT_ID)
);

CREATE TABLE TASK_ASSIGNEES
(
	ASSIGNEE_ID INT NOT NULL,
    TASK_ID INT NOT NULL,
    FOREIGN KEY FK_ASSIGNEE_ID (ASSIGNEE_ID) REFERENCES STAFF (STAFF_ID),
    FOREIGN KEY FK_TA_TASK_ID (TASK_ID) REFERENCES TASKS (TASK_ID),
    UNIQUE KEY (ASSIGNEE_ID, TASK_ID)
);

CREATE TABLE TASK_COMMENTS
(
	COMMENT_ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    COMMENT_MSG TEXT NOT NULL,
    COMMENT_DATE TIMESTAMP NOT NULL,
    COMMENTER_ID INT NOT NULL,
    TASK_ID INT NOT NULL,
    FOREIGN KEY FK_COMMENTER_ID (COMMENTER_ID) REFERENCES STAFF (STAFF_ID),
    FOREIGN KEY FK_TC_TASK_ID (TASK_ID) REFERENCES TASKS (TASK_ID)
);
