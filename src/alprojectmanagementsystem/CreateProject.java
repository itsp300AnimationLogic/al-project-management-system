package alprojectmanagementsystem;

/**
 * <h1>Create Project Screen</h1>
 * <p>This will be used to create new projects</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class CreateProject
{
    
    JFrame f;
    
    JLabel lblTitle,
           lblProjectName,
           lblProjectDescription,
           lblStartDate,
           lblEndDate,
           lblProjectOwner,
           lblSelectMembers;
    
    JTextField txfProjectName,
               txfStartDate,
               txfEndDate;
    
    JTextArea txaProjectDescription;
    
    JComboBox cbxProjectOwner,
              cbxSelectMembers;
    
    JScrollPane spForTxaProjectDescription,
                spForTblMembers;
    
    DefaultTableModel dtm;
    
    JTable tblMembers;
    
    JButton btnAddMember,
            btnCreateProject,
            btnBack;
    
    Connection c = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    Object tblMembersHeaders[] = 
    {
        "Staff ID",
        "First Name",
        "Last Name",
        "Email"
    };
    
    public CreateProject()
    {
        //Creates new frame
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 720); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new ManageProjectsHome().setVisible(true);
            }
        });
        
        lblTitle = new JLabel("CREATE PROJECT"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        lblProjectName = new JLabel("Project Name:");
        lblProjectName.setSize(lblProjectName.getPreferredSize());
        
        txfProjectName = new JTextField(30);
        txfProjectName.setSize(txfProjectName.getPreferredSize());
        
        lblProjectDescription = new JLabel("Enter a Description of the Project:");
        lblProjectDescription.setSize(lblProjectDescription.getPreferredSize());
        
        txaProjectDescription = new JTextArea();
        
        spForTxaProjectDescription = new JScrollPane(txaProjectDescription);
        spForTxaProjectDescription.setSize(txfProjectName.getWidth(), 50);
        
        lblStartDate = new JLabel("Start Date:");
        lblStartDate.setSize(lblStartDate.getPreferredSize());
        
        txfStartDate = new JTextField(30);
        txfStartDate.setSize(txfStartDate.getPreferredSize());
        txfStartDate.setToolTipText("DD/MM/YYYY");
        
        lblEndDate = new JLabel("End Date:");
        lblEndDate.setSize(lblEndDate.getPreferredSize());
        
        txfEndDate = new JTextField(30);
        txfEndDate.setSize(txfEndDate.getPreferredSize());
        txfEndDate.setToolTipText("DD/MM/YYYY");
        
        lblProjectOwner = new JLabel("Project Owner:");
        lblProjectOwner.setSize(lblProjectOwner.getPreferredSize());

        cbxProjectOwner = new JComboBox(fillComboBoxes());
        cbxProjectOwner.setSize(txfEndDate.getPreferredSize());
        cbxProjectOwner.setBackground(Color.white);
        
        lblSelectMembers = new JLabel("Add Members:");
        lblSelectMembers.setSize(lblSelectMembers.getPreferredSize());
        
        cbxSelectMembers = new JComboBox(fillComboBoxes());
        cbxSelectMembers.setSize(cbxProjectOwner.getWidth() - 65, cbxProjectOwner.getHeight());
        cbxSelectMembers.setBackground(Color.white);
        
        btnAddMember = new JButton("Add");
        btnAddMember.setSize(btnAddMember.getPreferredSize().width, cbxSelectMembers.getHeight());
        btnAddMember.setForeground(Color.black);
        btnAddMember.setBackground(Color.white);
        btnAddMember.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String selectedMember = cbxSelectMembers.getSelectedItem().toString();
                    selectedMember = selectedMember.substring(0, selectedMember.indexOf(" - "));
                    int iSelectedMember = Integer.parseInt(selectedMember);

                    boolean found = false;
                    
                    for (int i = 0; i < dtm.getRowCount(); i++)
                    {
                        if ((int) dtm.getValueAt(i, 0) == iSelectedMember)
                        {
                            found = true;
                        }
                    }

                    if (!found)
                    {
                        try
                        {
                            ps = c.prepareStatement("SELECT * FROM STAFF WHERE STAFF_ID = ?");
                            ps.setInt(1, iSelectedMember);
                            rs = ps.executeQuery();

                            if (rs.next());
                            {
                                Object row[] = 
                                {
                                    rs.getObject(1),
                                    rs.getObject(2),
                                    rs.getObject(3),
                                    rs.getObject(5)
                                };

                                dtm.addRow(row);
                            }
                            
                            rs.close();
                            ps.close();
                        }
                        catch (SQLException ex) { System.out.println(ex.getMessage()); }
                    }
                }
            }
        );

        setDTM();
        
        tblMembers = new JTable(dtm);
        tblMembers.setForeground(Color.black);
        tblMembers.setBackground(Color.white);
        tblMembers.setFillsViewportHeight(true);
        tblMembers.setRowHeight(25);
        tblMembers.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        spForTblMembers = new JScrollPane(tblMembers);
        spForTblMembers.setSize(txfProjectName.getWidth(), 120);
        
        btnCreateProject = new JButton("Create Project");
        btnCreateProject.setSize(btnCreateProject.getPreferredSize());
        btnCreateProject.setForeground(Color.black);
        btnCreateProject.setBackground(Color.white);
        btnCreateProject.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String projectName        = txfProjectName.getText(),
                           projectDescription = txaProjectDescription.getText(),
                           startDate          = txfStartDate.getText(),
                           endDate            = txfEndDate.getText(),
                           projectOwner       = cbxProjectOwner.getSelectedItem().toString();
                    
                    int projectOwnerID = Integer.parseInt(projectOwner.substring(0, projectOwner.indexOf(" - ")));
                    
                    ArrayList projectMembers = new ArrayList();
                    
                    for (int i = 0; i < tblMembers.getRowCount(); i++)
                    {
                        projectMembers.add(tblMembers.getValueAt(i, 0));
                    }
                    
                    Common cmn = new Common();
                    
                    if (cmn.project(projectName, projectDescription, startDate, endDate, projectOwnerID, projectMembers, -1, Common.PROJECTINSERT) == 0)
                    {
                        f.dispose();
                        new ManageProjectsHome().setVisible(true);
                    }
                    
                }
            }
        );

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new ManageProjectsHome().setVisible(true);
                }
            }
        );
        
        btnBack.setLocation(10, 10);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 40);
        lblProjectName.setLocation((f.getWidth() - txfProjectName.getWidth()) / 2, 120);
        txfProjectName.setLocation((f.getWidth() - txfProjectName.getWidth()) / 2, 140);
        lblProjectDescription.setLocation((f.getWidth() - spForTxaProjectDescription.getWidth()) / 2, 180);
        spForTxaProjectDescription.setLocation((f.getWidth() - spForTxaProjectDescription.getWidth()) / 2, 200);
        lblStartDate.setLocation((f.getWidth() - txfStartDate.getWidth()) / 2, 260);
        txfStartDate.setLocation((f.getWidth() - txfStartDate.getWidth()) / 2, 280);
        lblEndDate.setLocation((f.getWidth() - txfEndDate.getWidth()) / 2, 320);
        txfEndDate.setLocation((f.getWidth() - txfEndDate.getWidth()) / 2, 340);
        lblProjectOwner.setLocation((f.getWidth() - cbxProjectOwner.getWidth()) / 2, 380);
        cbxProjectOwner.setLocation((f.getWidth() - cbxProjectOwner.getWidth()) / 2, 400);
        lblSelectMembers.setLocation((f.getWidth() - cbxProjectOwner.getWidth()) / 2, 440);
        cbxSelectMembers.setLocation((f.getWidth() - cbxSelectMembers.getWidth() - btnAddMember.getWidth() - 10) / 2, 460);
        btnAddMember.setLocation(cbxSelectMembers.getLocation().x + cbxSelectMembers.getWidth() + 10, 460);
        spForTblMembers.setLocation((f.getWidth() - spForTblMembers.getWidth()) / 2, 500);
        btnCreateProject.setLocation((f.getWidth() - btnCreateProject.getWidth()) / 2, 640);

        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblProjectName);
        f.getContentPane().add(txfProjectName);
        f.getContentPane().add(lblProjectDescription);
        f.getContentPane().add(spForTxaProjectDescription);
        f.getContentPane().add(lblStartDate);
        f.getContentPane().add(txfStartDate);
        f.getContentPane().add(lblEndDate);
        f.getContentPane().add(txfEndDate);
        f.getContentPane().add(lblProjectOwner);
        f.getContentPane().add(cbxProjectOwner);
        f.getContentPane().add(lblSelectMembers);
        f.getContentPane().add(cbxSelectMembers);
        f.getContentPane().add(btnAddMember);
        f.getContentPane().add(spForTblMembers);
        f.getContentPane().add(btnCreateProject);

        f.setVisible(true);
    }
    
    private Vector<Object> fillComboBoxes()
    {
        Vector potentialOwners = new Vector();
        
        try
        {
            ps = c.prepareStatement("SELECT * FROM STAFF");
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                potentialOwners.add(rs.getObject(1) + " - " + rs.getObject(5));
            }
            
            rs.close();
            ps.close();
        }
        catch(SQLException ex) { System.out.println(ex.getMessage()); }
        
        return potentialOwners;
    }
    
    private void setDTM()
    {
        dtm = new DefaultTableModel(tblMembersHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
    }
    
}
