package alprojectmanagementsystem;

/**
 * <h1>Forgot Password Pop-up Screen</h1>
 * <p>This screen allows staff members to reset
 * their passwords with the email address
 * they used to create their account</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ResetPassword
{
    
    Common cmn = new Common();
    
    public ResetPassword()
    {
        //Creates a new frame
        JFrame f = new JFrame("AL Project Management System");
        //f.setIconImage(new ImageIcon("").getImage()); //Sets the icon for frame
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 640); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new Login();
            }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("RESET PASSWORD"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for email
        JLabel lblEmail = new JLabel("Email:");
        lblEmail.setSize(lblEmail.getPreferredSize());
        
        //Textfield for email to be inputted
        JTextField txfEmail = new JTextField(20);
        txfEmail.setSize(txfEmail.getPreferredSize());
        
        //Label for secret code
        JLabel lblSecretCode = new JLabel("Enter Your Secret Code:");
        lblSecretCode.setSize(lblSecretCode.getPreferredSize());
        
        //Textfield for secret code
        JTextField txfSecretCode = new JTextField(20);
        txfSecretCode.setSize(txfSecretCode.getPreferredSize());
        txfSecretCode.setEditable(false);
        
        //Label for password
        JLabel lblPassword = new JLabel("Enter New Password:");
        lblPassword.setSize(lblPassword.getPreferredSize());
        
        //Password field for a new password to be inputted
        JPasswordField pwfPassword = new JPasswordField(20);
        pwfPassword.setSize(pwfPassword.getPreferredSize());
        pwfPassword.setEditable(false);
        
        //Label for confirm password
        JLabel lblConfirm = new JLabel("Confirm New Password:");
        lblConfirm.setSize(lblConfirm.getPreferredSize());
        
        //Password field for a new password to be confirmed
        JPasswordField pwfConfirm = new JPasswordField(20);
        pwfConfirm.setSize(pwfConfirm.getPreferredSize());
        pwfConfirm.setEditable(false);
        
        //Button to reset password
        JButton btnReset = new JButton("Reset Password");
        btnReset.setSize(160, btnReset.getPreferredSize().height);
        btnReset.setBackground(Color.WHITE);
        btnReset.setForeground(Color.BLACK);
        btnReset.setVisible(false);
        btnReset.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String email      = txfEmail.getText(),
                       secretCode = txfSecretCode.getText(),
                       password   = "",
                       confirm    = "";
                
                char passwordArr[] = pwfPassword.getPassword(),
                     confirmArr[]  = pwfConfirm.getPassword();
                
                //Iterating through the char arrays to represent the passwords as Strings
                for (char c : passwordArr) password += c;
                for (char c : confirmArr) confirm += c;
                
                if (cmn.resetPassword(email, secretCode, password, confirm) == 0)
                {
                    f.dispose();
                    new Login();
                }
            }
        });
        
        //Button to cancel operation
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setSize(160, btnCancel.getPreferredSize().height);
        btnCancel.setBackground(Color.WHITE);
        btnCancel.setForeground(Color.BLACK);
        btnCancel.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new Login();
            }
        });

        //Button to send secret code to a staff member
        JButton btnSendSecretCode = new JButton("Email Secret Code");
        btnSendSecretCode.setSize(160, btnSendSecretCode.getPreferredSize().height);
        btnSendSecretCode.setBackground(Color.WHITE);
        btnSendSecretCode.setForeground(Color.BLACK);
        btnSendSecretCode.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String email = txfEmail.getText();

                    if (cmn.sendSecretCode(email) == 0)
                    {
                        txfEmail.setEditable(false);
                        btnSendSecretCode.setVisible(false);
                        
                        txfSecretCode.setEditable(true);
                        pwfPassword.setEditable(true);
                        pwfConfirm.setEditable(true);
                        btnReset.setVisible(true);
                    }
                }
            }
        );
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 80);
        lblEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 180);
        txfEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 200);
        lblSecretCode.setLocation((f.getWidth()-txfSecretCode.getWidth())/2, 240);
        txfSecretCode.setLocation((f.getWidth()-txfSecretCode.getWidth())/2, 260);
        btnSendSecretCode.setLocation((f.getWidth()-btnSendSecretCode.getWidth())/2, 460);
        lblPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 300);
        pwfPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 320);
        lblConfirm.setLocation((f.getWidth()-pwfConfirm.getWidth())/2, 360);
        pwfConfirm.setLocation((f.getWidth()-pwfConfirm.getWidth())/2, 380);
        btnReset.setLocation(((f.getWidth()-btnReset.getWidth())/2), 460);
        btnCancel.setLocation(((f.getWidth()-btnCancel.getWidth())/2), 500);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblEmail);
        f.getContentPane().add(txfEmail);
        f.getContentPane().add(lblSecretCode);
        f.getContentPane().add(txfSecretCode);
        f.getContentPane().add(btnSendSecretCode);
        f.getContentPane().add(lblPassword);
        f.getContentPane().add(pwfPassword);
        f.getContentPane().add(lblConfirm);
        f.getContentPane().add(pwfConfirm);
        f.getContentPane().add(btnReset);
        f.getContentPane().add(btnCancel);
        
        //Shows the frame
        f.setVisible(true);
    }
    
}
