package alprojectmanagementsystem;

/**
 * <h1>Update Project Screen</h1>
 * <p>This will be used to update projects</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.swing.*;

public class UpdateProject
{
    
    JFrame f;
    
    JLabel lblTitle,
           lblProjectName,
           lblProjectDescription,
           lblStartDate,
           lblEndDate,
           lblProjectOwner;
    
    JTextField txfProjectName,
               txfStartDate,
               txfEndDate;
    
    JTextArea txaProjectDescription;
    
    JComboBox cbxProjectOwner;
    
    JScrollPane spForTxaProjectDescription;
    
    JButton btnUpdateProject,
            btnBack;
    
    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public UpdateProject(int p_projectID)
    {
        //Creates new frame
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(480, 640);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new ManageProjectsHome().setVisible(true);
            }
        });
        
        lblTitle = new JLabel("UPDATE PROJECT");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        lblProjectName = new JLabel("Project Name:");
        lblProjectName.setSize(lblProjectName.getPreferredSize());
        
        txfProjectName = new JTextField(30);
        txfProjectName.setSize(txfProjectName.getPreferredSize());
        
        lblProjectDescription = new JLabel("Enter a Description of the Project:");
        lblProjectDescription.setSize(lblProjectDescription.getPreferredSize());
        
        txaProjectDescription = new JTextArea();
        
        spForTxaProjectDescription = new JScrollPane(txaProjectDescription);
        spForTxaProjectDescription.setSize(txfProjectName.getWidth(), 50);
        
        lblStartDate = new JLabel("Start Date:");
        lblStartDate.setSize(lblStartDate.getPreferredSize());
        
        txfStartDate = new JTextField(30);
        txfStartDate.setSize(txfStartDate.getPreferredSize());
        txfStartDate.setToolTipText("DD/MM/YYYY");
        
        lblEndDate = new JLabel("End Date:");
        lblEndDate.setSize(lblEndDate.getPreferredSize());
        
        txfEndDate = new JTextField(30);
        txfEndDate.setSize(txfEndDate.getPreferredSize());
        txfEndDate.setToolTipText("DD/MM/YYYY");
        
        lblProjectOwner = new JLabel("Project Owner:");
        lblProjectOwner.setSize(lblProjectOwner.getPreferredSize());

        cbxProjectOwner = new JComboBox(fillComboBoxes());
        cbxProjectOwner.setSize(txfEndDate.getPreferredSize());
        cbxProjectOwner.setBackground(Color.white);
        
        btnUpdateProject = new JButton("Update Project");
        btnUpdateProject.setSize(btnUpdateProject.getPreferredSize());
        btnUpdateProject.setForeground(Color.black);
        btnUpdateProject.setBackground(Color.white);
        btnUpdateProject.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    String projectName        = txfProjectName.getText(),
                           projectDescription = txaProjectDescription.getText(),
                           startDate          = txfStartDate.getText(),
                           endDate            = txfEndDate.getText(),
                           projectOwner       = cbxProjectOwner.getSelectedItem().toString();
                    
                    int projectOwnerID = Integer.parseInt(projectOwner.substring(0, projectOwner.indexOf(" - ")));
                    
                    Common cmn = new Common();
                    
                    if (cmn.project(projectName, projectDescription, startDate, endDate, projectOwnerID, null, p_projectID, Common.PROJECTUPDATE) == 0)
                    {
                        f.dispose();
                        new ManageProjectsHome().setVisible(true);
                    }
                    
                }
            }
        );
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT p.*, s.STAFF_ID, s.EMAIL " + 
                "FROM PROJECTS p, STAFF s " +
                "WHERE PROJECT_ID = ? AND s.STAFF_ID = p.OWNER_ID"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                txfProjectName.setText(rs.getString(2));
                txaProjectDescription.setText(rs.getString(3));
                
                Date startDate = new Date(rs.getDate(4).getTime()),
                     endDate   = new Date(rs.getDate(5).getTime());
                
                txfStartDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(startDate));
                txfEndDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(endDate));
                cbxProjectOwner.setSelectedItem(rs.getInt(7) + " - " + rs.getString(8));
            }
        }
        catch (Exception e) { JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new ManageProjectsHome().setVisible(true);
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 60);
        lblProjectName.setLocation((f.getWidth() - txfProjectName.getWidth()) / 2, 140);
        txfProjectName.setLocation((f.getWidth() - txfProjectName.getWidth()) / 2, 160);
        lblProjectDescription.setLocation((f.getWidth() - spForTxaProjectDescription.getWidth()) / 2, 200);
        spForTxaProjectDescription.setLocation((f.getWidth() - spForTxaProjectDescription.getWidth()) / 2, 220);
        lblStartDate.setLocation((f.getWidth() - txfStartDate.getWidth()) / 2, 280);
        txfStartDate.setLocation((f.getWidth() - txfStartDate.getWidth()) / 2, 300);
        lblEndDate.setLocation((f.getWidth() - txfEndDate.getWidth()) / 2, 340);
        txfEndDate.setLocation((f.getWidth() - txfEndDate.getWidth()) / 2, 360);
        lblProjectOwner.setLocation((f.getWidth() - cbxProjectOwner.getWidth()) / 2, 400);
        cbxProjectOwner.setLocation((f.getWidth() - cbxProjectOwner.getWidth()) / 2, 420);
        btnUpdateProject.setLocation((f.getWidth() - btnUpdateProject.getWidth()) / 2, 500);

        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblProjectName);
        f.getContentPane().add(txfProjectName);
        f.getContentPane().add(lblProjectDescription);
        f.getContentPane().add(spForTxaProjectDescription);
        f.getContentPane().add(lblStartDate);
        f.getContentPane().add(txfStartDate);
        f.getContentPane().add(lblEndDate);
        f.getContentPane().add(txfEndDate);
        f.getContentPane().add(lblProjectOwner);
        f.getContentPane().add(cbxProjectOwner);
        f.getContentPane().add(btnUpdateProject);

        f.setVisible(true);
    }
    
    private Vector<Object> fillComboBoxes()
    {
        Vector potentialOwners = new Vector();
        
        try
        {
            ps = dbConnection.prepareStatement("SELECT * FROM STAFF");
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                potentialOwners.add(rs.getObject(1) + " - " + rs.getObject(5));
            }
            
            rs.close();
            ps.close();
        }
        catch(SQLException ex) { System.out.println(ex.getMessage()); }
        
        return potentialOwners;
    }
    
}
