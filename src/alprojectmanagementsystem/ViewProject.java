package alprojectmanagementsystem;

/**
 * <h1>Project Screen</h1>
 * <p>Used to create tasks 
 * and leave messages</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ViewProject
{
    
    JFrame f;
    
    JLabel lblTitle;
    
    JPanel pnlTasks,
           pnlChat,
           pnlScrollableChat,
           pnlProjectDetails;
    
    JScrollPane spForTblTasks,
                spForChat,
                spForTxaMessage;
    
    DefaultTableModel dtmTasks;
    
    JTable tblTasks;
    
    JTextArea txaMessage;
    
    JButton btnCreateTask,
            btnViewTask,
            btnSendMessage,
            btnBack;

    JLabel lblProjectDetails[][];
    
    Common cmn = new Common();

    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;

    int messageCount = 0;
    
    public ViewProject(int    p_projectID,
                       String p_source)
    {
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(800, 820);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                if (p_source.equals("Admin")) new ManageProjectsHome().setVisible(true);
                else new UserDashboard();
            }
        });
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT PROJECT_NAME FROM PROJECTS " +
                "WHERE PROJECT_ID = ?"
            );
            ps.setInt(1, p_projectID);
            
            rs = ps.executeQuery();
            
            lblTitle = new JLabel(rs.next() ? rs.getString(1).toUpperCase() : "PROJECT");
            
            rs.close();
            ps.close();
        }
        catch (SQLException se) {}

        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());
        
        pnlTasks = new JPanel();
        pnlTasks.setLayout(null);
        pnlTasks.setBorder(BorderFactory.createTitledBorder("Tasks:"));
        pnlTasks.setSize(300, 400);
        pnlTasks.setBackground(Color.white);

        setDTM(p_projectID);
        
        tblTasks = new JTable(dtmTasks);
        tblTasks.setForeground(Color.black);
        tblTasks.setBackground(Color.white);
        tblTasks.setFillsViewportHeight(true);
        tblTasks.setRowHeight(25);
        tblTasks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        spForTblTasks = new JScrollPane(tblTasks);
        spForTblTasks.setSize(pnlTasks.getWidth() - 20, pnlTasks.getHeight() - 110);
        
        btnCreateTask = new JButton("Create Task");
        btnCreateTask.setSize(spForTblTasks.getWidth(), btnCreateTask.getPreferredSize().height);
        btnCreateTask.setForeground(Color.black);
        btnCreateTask.setBackground(Color.white);
        btnCreateTask.setEnabled(p_source.equals("User"));
        btnCreateTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (p_source.equals("User"))
                    {
                        int staffID = Integer.parseInt(cmn.getStaffID());

                        f.dispose();
                        new CreateTask(staffID, p_projectID, p_source);
                    }
                }
            }
        );
        
        btnViewTask = new JButton("View Task");
        btnViewTask.setSize(spForTblTasks.getWidth(), btnViewTask.getPreferredSize().height);
        btnViewTask.setForeground(Color.black);
        btnViewTask.setBackground(Color.white);
        btnViewTask.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    int selectedTask = tblTasks.getSelectedRow(),
                        taskID;

                    if (selectedTask > -1)
                    {
                        taskID = (int) tblTasks.getValueAt(selectedTask, 0);
                        f.dispose();
                        new ViewTask(taskID, p_projectID, p_source);
                    }
                }
            }
        );
        
        pnlChat = new JPanel();
        pnlChat.setLayout(null);
        pnlChat.setBorder(BorderFactory.createTitledBorder("Chat:"));
        pnlChat.setSize(300, 400);
        pnlChat.setBackground(Color.white);
        
        pnlScrollableChat = new JPanel();
        pnlScrollableChat.setLayout(new GridLayout(0, 1));
        pnlScrollableChat.setBackground(Color.white);
        
        spForChat = new JScrollPane();
        spForChat.setSize(pnlChat.getWidth() - 20, pnlChat.getHeight() - 80);
        spForChat.getViewport().add(pnlScrollableChat);
        spForChat.getViewport().setBackground(Color.white);
        
        btnSendMessage = new JButton("Send");
        btnSendMessage.setSize(btnSendMessage.getPreferredSize().width, btnSendMessage.getPreferredSize().height + 10);
        btnSendMessage.setForeground(Color.black);
        btnSendMessage.setBackground(Color.white);
        btnSendMessage.setEnabled(p_source.equals("User"));
        btnSendMessage.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    if (p_source.equals("User"))
                    {
                        String message = txaMessage.getText();

                        if (cmn.sendMessageInChat(message, p_projectID) == 0)
                        {
                            txaMessage.setText("");
                            getMessagesForCurrentProject(p_projectID);
                        }
                    }
                }
            }
        );
        
        txaMessage = new JTextArea();
        
        spForTxaMessage = new JScrollPane(txaMessage, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        spForTxaMessage.setSize(spForChat.getWidth() - btnSendMessage.getWidth() - 10, btnSendMessage.getHeight());

        pnlProjectDetails = new JPanel();
        pnlProjectDetails.setLayout(new GridLayout(6, 2));
        pnlProjectDetails.setBorder(BorderFactory.createTitledBorder("Project Details:"));
        pnlProjectDetails.setSize(pnlTasks.getWidth() * (5 / 2) + 40, 200);
        pnlProjectDetails.setBackground(Color.white);

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    if (p_source.equals("Admin")) new ManageProjectsHome().setVisible(true);
                    else new UserDashboard();
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 40);
        pnlTasks.setLocation(((f.getWidth() - pnlTasks.getWidth()) / 2) - pnlTasks.getWidth() / 2 - 20, 120);
        pnlChat.setLocation(((f.getWidth() - pnlChat.getWidth()) / 2) + pnlChat.getWidth() / 2 + 20, 120);
        spForTblTasks.setLocation((pnlTasks.getWidth() - spForTblTasks.getWidth()) / 2, 20);
        btnCreateTask.setLocation((pnlTasks.getWidth() - spForTblTasks.getWidth()) / 2, spForTblTasks.getY() + spForTblTasks.getHeight() + 10);
        btnViewTask.setLocation((pnlTasks.getWidth() - spForTblTasks.getWidth()) / 2, spForTblTasks.getY() + spForTblTasks.getHeight() + 10 + btnCreateTask.getHeight() + 10);
        spForChat.setLocation((pnlChat.getWidth() - spForChat.getWidth()) / 2, 20);
        spForTxaMessage.setLocation(spForChat.getX(), spForChat.getY() + spForChat.getHeight() + 10);
        btnSendMessage.setLocation(spForTxaMessage.getX() + spForTxaMessage.getWidth() + 10, spForTxaMessage.getY());
        pnlProjectDetails.setLocation((f.getWidth() - pnlProjectDetails.getWidth()) / 2, pnlTasks.getY() + pnlTasks.getHeight() + 20);
        
        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(pnlTasks);
        f.getContentPane().add(pnlChat);
        pnlTasks.add(spForTblTasks);
        pnlTasks.add(btnCreateTask);
        pnlTasks.add(btnViewTask);
        pnlChat.add(spForChat);
        pnlChat.add(spForTxaMessage);
        pnlChat.add(btnSendMessage);
        f.getContentPane().add(pnlProjectDetails);

        setProjectDetails(p_projectID);

        pnlProjectDetails.add(lblProjectDetails[0][0]);
        pnlProjectDetails.add(lblProjectDetails[0][1]);
        pnlProjectDetails.add(lblProjectDetails[1][0]);
        pnlProjectDetails.add(lblProjectDetails[1][1]);
        pnlProjectDetails.add(lblProjectDetails[2][0]);
        pnlProjectDetails.add(lblProjectDetails[2][1]);
        pnlProjectDetails.add(lblProjectDetails[3][0]);
        pnlProjectDetails.add(lblProjectDetails[3][1]);
        pnlProjectDetails.add(lblProjectDetails[4][0]);
        pnlProjectDetails.add(lblProjectDetails[4][1]);
        pnlProjectDetails.add(lblProjectDetails[5][0]);
        pnlProjectDetails.add(lblProjectDetails[5][1]);
        
        getMessagesForCurrentProject(p_projectID);
        refreshChat(p_projectID);

        repaintChat();

        f.setVisible(true);
    }
    
    void setDTM(int p_projectID)
    {
        Object tblTasksHeaders[] = 
        {
            "Task ID",
            "Task Name"
        };
        
        dtmTasks = new DefaultTableModel(tblTasksHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT TASK_ID, TASK_NAME FROM TASKS " + 
                "WHERE PROJECT_ID = ?"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();

            while (rs.next())
            {
                Object task[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2)
                };

                dtmTasks.addRow(task);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setProjectDetails(int p_projectID)
    {
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT p.*, s.FIRST_NAME, s.LAST_NAME " + 
                "FROM PROJECTS p, STAFF s " +
                "WHERE PROJECT_ID = ? AND s.STAFF_ID = p.OWNER_ID"
            );
            ps.setInt(1, p_projectID);

            rs = ps.executeQuery();

            if (rs.next())
            {
                lblProjectDetails = new JLabel[6][2];
                
                lblProjectDetails[0][0] = new JLabel("ID:");
                lblProjectDetails[1][0] = new JLabel("Name:");
                lblProjectDetails[2][0] = new JLabel("Description:");
                lblProjectDetails[3][0] = new JLabel("Start Date:");
                lblProjectDetails[4][0] = new JLabel("End Date:");
                lblProjectDetails[5][0] = new JLabel("Owner:");

                for (int i = 0; i < lblProjectDetails.length; i++)
                {
                    lblProjectDetails[i][0].setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
                }
                
                lblProjectDetails[0][1] = new JLabel(rs.getObject(1).toString());
                lblProjectDetails[1][1] = new JLabel(rs.getObject(2).toString());
                lblProjectDetails[2][1] = new JLabel(rs.getObject(3).toString());
                lblProjectDetails[3][1] = new JLabel(rs.getObject(4).toString());
                lblProjectDetails[4][1] = new JLabel(rs.getObject(5).toString());
                lblProjectDetails[5][1] = new JLabel(rs.getObject(7) + " " + rs.getObject(8));
            }
        }
        catch (SQLException se) {}
    }
    
    void getMessagesForCurrentProject(int p_projectID)
    {
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT pc.*, s.FIRST_NAME, s.LAST_NAME " + 
                "FROM PROJECT_CHATROOM pc, STAFF s " +
                "WHERE pc.PROJECT_ID = ? AND s.STAFF_ID = pc.MESSENGER_ID " +
                "ORDER BY pc.MESSAGE_ID ASC"
            );
            ps.setInt(1, p_projectID);
            
            rs = ps.executeQuery();
            
            // Get the row count
            rs.last();
            int rowCount = rs.getRow();
            rs.beforeFirst();
            
            JLabel lblMembers[]  = new JLabel[rowCount],
                   lblTimes[]    = new JLabel[rowCount],
                   lblMessages[] = new JLabel[rowCount];
            
            JPanel pnlForMessages[]    = new JPanel[rowCount],
                   pnlForMessageInfo[] = new JPanel[rowCount];

            if (rowCount > messageCount)
            {
                messageCount = rowCount;
                pnlScrollableChat.removeAll();

                while (rs.next())
                {
                    int rowNumber = rs.getRow() - 1;

                    pnlForMessages[rowNumber] = new JPanel();
                    pnlForMessages[rowNumber].setLayout(new GridLayout(2, 1));
                    pnlForMessages[rowNumber].setSize(spForChat.getWidth(), pnlForMessages[rowNumber].getPreferredSize().height);
                    pnlForMessages[rowNumber].setBackground(Color.white);
                    pnlForMessages[rowNumber].setBorder(BorderFactory.createEtchedBorder());

                    pnlForMessageInfo[rowNumber] = new JPanel();
                    pnlForMessageInfo[rowNumber].setLayout(new GridLayout(1, 2));
                    pnlForMessageInfo[rowNumber].setBackground(Color.white);
                    
                    lblMembers[rowNumber] = new JLabel(rs.getString(6) + " " + rs.getString(7));
                    lblMembers[rowNumber].setSize(lblMembers[rowNumber].getPreferredSize());
                    lblMembers[rowNumber].setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
                    lblMembers[rowNumber].setForeground(Color.blue);
                    
                    lblTimes[rowNumber] = new JLabel(rs.getTimestamp(3).toString().substring(0, rs.getTimestamp(3).toString().length() - 2));
                    lblTimes[rowNumber].setSize(lblTimes[rowNumber].getPreferredSize());
                    lblTimes[rowNumber].setForeground(Color.black);
                    
                    lblMessages[rowNumber] = new JLabel(rs.getString(2));
                    lblMessages[rowNumber].setSize(lblMessages[rowNumber].getPreferredSize());
                    lblMessages[rowNumber].setForeground(Color.black);
                    lblMessages[rowNumber].setBorder(BorderFactory.createEmptyBorder(0, 7, 0, 0));
                    
                    pnlForMessageInfo[rowNumber].add(lblMembers[rowNumber]);
                    pnlForMessageInfo[rowNumber].add(lblTimes[rowNumber]);
                    
                    pnlForMessages[rowNumber].add(pnlForMessageInfo[rowNumber]);
                    pnlForMessages[rowNumber].add(lblMessages[rowNumber]);

                    pnlForMessages[rowNumber].setSize(pnlScrollableChat.getWidth(), pnlForMessages[rowNumber].getMinimumSize().height);

                    pnlScrollableChat.add(pnlForMessages[rowNumber]);
                }

                repaintChat();
            }
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    final void refreshChat(int p_projectID)
    {
        new Timer
        (
            1000,
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent ae)
                {
                    getMessagesForCurrentProject(p_projectID);
                }
            }
        ).start();
    }

    void repaintChat()
    {
        pnlChat.remove(spForChat);
        spForChat = new JScrollPane();
        spForChat.setSize(pnlChat.getWidth() - 20, pnlChat.getHeight() - 80);
        spForChat.getViewport().add(pnlScrollableChat);
        spForChat.getViewport().setBackground(Color.white);
        spForChat.setLocation((pnlChat.getWidth() - spForChat.getWidth()) / 2, 20);
        pnlChat.add(spForChat);
        
        pnlScrollableChat.repaint();
        pnlScrollableChat.revalidate();
    }
    
}
