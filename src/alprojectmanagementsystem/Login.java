package alprojectmanagementsystem;

/**
 * <h1>Login Screen</h1>
 * <p>The is the starting point of the program.
 * Staff members will fill in their credentials
 * and gain access to the system if their account
 * exists</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Login
{
    
    Common c1 = new Common();
    
    public Login()
    {
        //Variables to determine database connection status
        boolean isDatabaseConnected = c1.isDatabaseConnected(Common.getConnection());
        String databaseStatus = isDatabaseConnected ? "Connected" : "Not Connected";
        
        //Creates a new frame
        JFrame f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //Sets the default action when the close button is pressed
        f.setLayout(null); //Not setting a layout so components can be set manually
        f.setSize(480, 640); //Sets the size of the frame
        f.setResizable(false); //Makes it so that the frame cannot be resized
        f.setLocationRelativeTo(null); //This makes sure the frame starts at the center of the screen
        f.getContentPane().setBackground(Color.WHITE); //Sets the background color to white
        f.addWindowListener(new WindowAdapter() //Adds a window listener to listen for the windowClosing event
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                if (JOptionPane.showConfirmDialog(null, "Are you sure you want to exit ?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }
            }
        });
        
        //Label for title
        JLabel lblTitle = new JLabel("WELCOME"); //Creates a new label
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40)); //Sets the font and font size to be used
        lblTitle.setSize(lblTitle.getPreferredSize()); //Uses the default size of the label
        
        //Label for email
        JLabel lblEmail = new JLabel("Email:");
        lblEmail.setSize(lblEmail.getPreferredSize());
        
        //Textfield for email to be inputted
        JTextField txfEmail = new JTextField(20); //Creates a new textfield fit for 20 characters
        txfEmail.setSize(txfEmail.getPreferredSize()); //Uses the default size of the textfield
        
        //Label for password
        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setSize(lblPassword.getPreferredSize());
        
        //Special password field for user's password
        JPasswordField pwfPassword = new JPasswordField(20);
        pwfPassword.setSize(pwfPassword.getPreferredSize());
        
        //Label to reset password
        JLabel lblForgot = new JLabel("I forgot my password");
        lblForgot.setSize(lblForgot.getPreferredSize());
        lblForgot.setForeground(Color.BLACK);
        lblForgot.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new ResetPassword();
            }

            @Override
            public void mouseEntered(MouseEvent me) { lblForgot.setForeground(Color.BLUE); }

            @Override
            public void mouseExited(MouseEvent me) { lblForgot.setForeground(Color.BLACK); }
        });
        
        //Login button to authenticate user to either enter the admin frame or the store
        JButton btnLogin = new JButton("Login");
        btnLogin.setSize(160, btnLogin.getPreferredSize().height);
        btnLogin.setBackground(Color.WHITE);
        btnLogin.setForeground(Color.BLACK);
        btnLogin.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                //Extracting data from the components
                String email    = txfEmail.getText(),
                       password = "";
                
                char passwordArr[] = pwfPassword.getPassword();
                
                //Iterating through the char array to represent the password as a String
                for (char c : passwordArr) password += c;
                
                if (c1.login(email, password) == 0)
                {
                    f.dispose();
                }
            }
        });
        
        //Label to sign up a new staff member
        JLabel lblSignUp = new JLabel("No account ? Sign up !");
        lblSignUp.setSize(lblSignUp.getPreferredSize());
        lblSignUp.setForeground(Color.BLACK);
        lblSignUp.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent me)
            {
                f.dispose();
                new CreateAccount();
            }

            @Override
            public void mouseEntered(MouseEvent me) { lblSignUp.setForeground(Color.BLUE); }

            @Override
            public void mouseExited(MouseEvent me) { lblSignUp.setForeground(Color.BLACK); }
        });
        
        //Label to show the user what the status of the database is
        JLabel lblDatabaseStatus = new JLabel("Database Status: " + databaseStatus);
        lblDatabaseStatus.setSize(lblDatabaseStatus.getPreferredSize());
        lblDatabaseStatus.setForeground(isDatabaseConnected ? Color.GREEN : Color.RED);
        
        //Sets the location of all components
        lblTitle.setLocation((f.getWidth()-lblTitle.getWidth())/2, 80);
        lblEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 220);
        txfEmail.setLocation((f.getWidth()-txfEmail.getWidth())/2, 240);
        lblPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 280);
        pwfPassword.setLocation((f.getWidth()-pwfPassword.getWidth())/2, 300);
        lblForgot.setLocation((f.getWidth()-lblForgot.getWidth())/2, 340);
        btnLogin.setLocation((f.getWidth()-btnLogin.getWidth())/2, 460);
        lblSignUp.setLocation((f.getWidth()-lblSignUp.getWidth())/2, 500);
        lblDatabaseStatus.setLocation(10, 590);
        
        //Adds the components to the frame
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(lblEmail);
        f.getContentPane().add(txfEmail);
        f.getContentPane().add(lblPassword);
        f.getContentPane().add(pwfPassword);
        f.getContentPane().add(lblForgot);
        f.getContentPane().add(btnLogin);
        f.getContentPane().add(lblSignUp);
        f.getContentPane().add(lblDatabaseStatus);
        
        //Shows the frame
        f.setVisible(true);
    }
}
