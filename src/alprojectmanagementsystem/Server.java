package alprojectmanagementsystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;

/**
 *
 * @author Eynar Roshev [eynaroshev@gmail.com]
 */
public class Server {

    private ObjectOutputStream output;
    private ObjectInputStream input;
    private String clientRequest;
    private ServerSocket server;
    private Socket connection;
    private int clientCount = 0;
    private LinkedList<ClientConnection> clients;

    public static void main(String[] args) {
        Server server = new Server();
        server.runServer();
    }

    public void runServer() {
        try {
            clients = new LinkedList<>();
            server = new ServerSocket(12345, 100);
            System.out.println("Server is running...");
            while (true) {
                connection = server.accept();
                clients.add(new ClientConnection(clientCount++, connection));
                new Thread(() -> {
                    try {
                        output = new ObjectOutputStream(connection.getOutputStream());
                        output.flush();
                        input = new ObjectInputStream(connection.getInputStream());
                        clientRequest = (String) input.readObject();
                        output.flush();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
