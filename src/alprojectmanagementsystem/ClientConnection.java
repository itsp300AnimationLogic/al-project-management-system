package alprojectmanagementsystem;

import java.net.Socket;

/**
 *
 * @author Eynar Roshev [eynaroshev@gmail.com]
 */
public class ClientConnection {
    private int id;
    private Socket connection;

    public ClientConnection(int id, Socket connection) {
        this.id = id;
        this.connection = connection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Socket getConnection() {
        return connection;
    }

    public void setConnection(Socket connection) {
        this.connection = connection;
    }
    
    
}
