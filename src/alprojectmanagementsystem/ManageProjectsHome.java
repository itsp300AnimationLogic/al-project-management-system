
package alprojectmanagementsystem;

import java.awt.Color;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.plaf.basic.BasicProgressBarUI;
import javax.swing.table.DefaultTableModel;

public class ManageProjectsHome extends javax.swing.JFrame
{
    public ManageProjectsHome()
    {
        initComponents();
        
        this.getContentPane().setBackground(Color.white);
        this.setLocationRelativeTo(null);
        
        pbProjectProgress.setUI
        (
            new BasicProgressBarUI()
            {
                @Override
                protected Color getSelectionBackground() { return Color.black; }
                
                @Override
                protected Color getSelectionForeground() { return Color.black; }
            }
        );
        
        pbProjectStatus.setUI
        (
            new BasicProgressBarUI()
            {
                @Override
                protected Color getSelectionBackground() { return Color.black; }
                
                @Override
                protected Color getSelectionForeground() { return Color.black; }
            }
        );
        
        setTblProjects();
    }
    
    Common cmn = new Common();
    
    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    DefaultTableModel dtmProjects,
                      dtmMembers;
    
    void setTblProjects()
    {
        Object tblProjectsHeaders[] = 
        {
            "No",
            "Name",
            "Description",
            "Start Date",
            "End Date",
            "Owner"
        };
        
        dtmProjects = new DefaultTableModel(tblProjectsHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT p.*, s.FIRST_NAME, s.LAST_NAME " +
                "FROM PROJECTS p, STAFF s " +
                "WHERE s.STAFF_ID = p.OWNER_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object project[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(7) + " " + rs.getObject(8)
                };

                dtmProjects.addRow(project);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        tblProjects.setModel(dtmProjects);
    }
    
    void setTblProjectMembers(int p_projectID)
    {
        Object tblProjectMembersHeaders[] = 
        {
            "Member",
            "Last Seen"
        };
        
        dtmMembers = new DefaultTableModel(tblProjectMembersHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT * FROM PROJECT_MEMBERS " +
                "WHERE PROJECT_ID = ?"
            );
            ps.setInt(1, p_projectID);
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                int staffID = rs.getInt(1);
                
                PreparedStatement newPS = dbConnection.prepareStatement
                (
                    "SELECT FIRST_NAME, LAST_NAME " +
                    "FROM STAFF " + 
                    "WHERE STAFF_ID = ?"
                );
                newPS.setInt(1, staffID);
                
                ResultSet newRS = newPS.executeQuery();
                
                String fullName = newRS.next() ? newRS.getString(1) + " " + newRS.getString(2) : "";
                
                newRS.close();
                newPS.close();
                
                newPS = dbConnection.prepareStatement
                (
                    "SELECT END_TIME " +
                    "FROM SESSIONS " +
                    "WHERE STAFF_MEMBER_ID = ? " + 
                    "ORDER BY SESSION_ID ASC"
                );
                newPS.setInt(1, staffID);
                
                newRS = newPS.executeQuery();
                
                Object lastSeen = newRS.last() ? newRS.getObject(1) : new Object();
                
                Object projectMember[] = 
                {
                    fullName,
                    lastSeen
                };
                
                dtmMembers.addRow(projectMember);
                
                newRS.close();
                newPS.close();
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }

        tblProjectMembers.setModel(dtmMembers);
    }
    
    void setProjectStatus(int p_projectID)
    {
        int percentComplete = 0,
            tasksComplete   = 0,
            totalTasks      = 0;
        
        Color projectStatusColor = null;
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT SUM(COMPLETE) AS totalComplete, " +
                "COUNT(*) AS totalTasks " +
                "FROM TASKS WHERE PROJECT_ID = ?"
            );
            ps.setInt(1, p_projectID);
            
            rs = ps.executeQuery();
            
            if (rs.next())
            {
                tasksComplete = rs.getInt(1);
                totalTasks = rs.getInt(2);
                
                percentComplete = totalTasks > 0 ? (int) Math.round((float) tasksComplete / totalTasks * 100) : 0;
            }
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
        
        projectStatusColor = percentComplete >=  0 && percentComplete <= 20 ? Color.red
                           : percentComplete >= 21 && percentComplete <= 60 ? Color.orange
                           : percentComplete >= 61 && percentComplete <= 99 ? Color.yellow
                           : Color.green;
        
        pbProjectProgress.setString(percentComplete + "% Completed");
        pbProjectProgress.setValue(percentComplete);
        pbProjectProgress.setForeground(projectStatusColor);
        
        pbProjectStatus.setString(tasksComplete + " / " + totalTasks + " Tasks Completed");
        pbProjectStatus.setValue(100);
        pbProjectStatus.setForeground(projectStatusColor);
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        lblManageProjects = new javax.swing.JLabel();
        pnlProjectView = new javax.swing.JPanel();
        lblSelectedProject = new javax.swing.JLabel();
        pbProjectProgress = new javax.swing.JProgressBar();
        pbProjectStatus = new javax.swing.JProgressBar();
        btnViewProject = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblProjectMembers = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblProjects = new javax.swing.JTable();
        btnCreateProject = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        btnViewDatabase = new javax.swing.JButton();
        btnUpdateProject = new javax.swing.JButton();
        btnDeleteProject = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("AL Project Management System");
        setBackground(new java.awt.Color(255, 255, 255));
        setResizable(false);
        setSize(new java.awt.Dimension(794, 410));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jLayeredPane1.setBackground(new java.awt.Color(255, 255, 255));
        jLayeredPane1.setForeground(new java.awt.Color(255, 255, 255));
        jLayeredPane1.setPreferredSize(new java.awt.Dimension(900, 540));
        jLayeredPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblManageProjects.setFont(new java.awt.Font("Calibri", 0, 28)); // NOI18N
        lblManageProjects.setForeground(new java.awt.Color(0, 0, 0));
        lblManageProjects.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblManageProjects.setText("Manage Projects");
        jLayeredPane1.add(lblManageProjects, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, 240, 50));

        pnlProjectView.setBackground(new java.awt.Color(255, 255, 255));
        pnlProjectView.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlProjectView.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblSelectedProject.setFont(new java.awt.Font("Calibri", 0, 18)); // NOI18N
        lblSelectedProject.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSelectedProject.setText("Selected Project Name");
        lblSelectedProject.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pnlProjectView.add(lblSelectedProject, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 240, 40));

        pbProjectProgress.setBackground(new java.awt.Color(255, 255, 255));
        pbProjectProgress.setForeground(new java.awt.Color(255, 255, 255));
        pbProjectProgress.setString("0% Completed");
        pbProjectProgress.setStringPainted(true);
        pnlProjectView.add(pbProjectProgress, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 220, 23));

        pbProjectStatus.setBackground(new java.awt.Color(255, 255, 255));
        pbProjectStatus.setForeground(new java.awt.Color(255, 255, 255));
        pbProjectStatus.setString("Status");
        pbProjectStatus.setStringPainted(true);
        pnlProjectView.add(pbProjectStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 360, 220, 23));

        btnViewProject.setBackground(new java.awt.Color(255, 255, 255));
        btnViewProject.setFont(btnViewProject.getFont());
        btnViewProject.setForeground(new java.awt.Color(0, 0, 0));
        btnViewProject.setText("View Project");
        btnViewProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewProjectActionPerformed(evt);
            }
        });
        pnlProjectView.add(btnViewProject, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 400, 220, 30));

        tblProjectMembers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Member", "Last Seen"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblProjectMembers.setFillsViewportHeight(true);
        tblProjectMembers.setRowHeight(25);
        tblProjectMembers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(tblProjectMembers);

        pnlProjectView.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 240, 260));

        jLayeredPane1.add(pnlProjectView, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 80, 240, 440));

        tblProjects.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblProjects.setFillsViewportHeight(true);
        tblProjects.setRowHeight(25);
        tblProjects.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblProjects.getTableHeader().setReorderingAllowed(false);
        tblProjects.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblProjectsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblProjects);
        tblProjects.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tblProjects.getColumnModel().getColumnCount() > 0) {
            tblProjects.getColumnModel().getColumn(0).setResizable(false);
            tblProjects.getColumnModel().getColumn(1).setResizable(false);
            tblProjects.getColumnModel().getColumn(2).setResizable(false);
            tblProjects.getColumnModel().getColumn(3).setResizable(false);
            tblProjects.getColumnModel().getColumn(4).setResizable(false);
        }

        jLayeredPane1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 600, 400));

        btnCreateProject.setBackground(new java.awt.Color(255, 255, 255));
        btnCreateProject.setFont(btnCreateProject.getFont());
        btnCreateProject.setForeground(new java.awt.Color(0, 0, 0));
        btnCreateProject.setText("Create Project");
        btnCreateProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateProjectActionPerformed(evt);
            }
        });
        jLayeredPane1.add(btnCreateProject, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 70, 130, 40));

        btnLogout.setBackground(new java.awt.Color(255, 255, 255));
        btnLogout.setFont(btnLogout.getFont());
        btnLogout.setForeground(new java.awt.Color(0, 0, 0));
        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });
        jLayeredPane1.add(btnLogout, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 130, 40));

        btnViewDatabase.setBackground(new java.awt.Color(255, 255, 255));
        btnViewDatabase.setFont(btnViewDatabase.getFont());
        btnViewDatabase.setForeground(new java.awt.Color(0, 0, 0));
        btnViewDatabase.setText("View Database");
        btnViewDatabase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewDatabaseActionPerformed(evt);
            }
        });
        jLayeredPane1.add(btnViewDatabase, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 20, 160, 40));

        btnUpdateProject.setBackground(new java.awt.Color(255, 255, 255));
        btnUpdateProject.setFont(btnUpdateProject.getFont());
        btnUpdateProject.setForeground(new java.awt.Color(0, 0, 0));
        btnUpdateProject.setText("Update Project");
        btnUpdateProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateProjectActionPerformed(evt);
            }
        });
        jLayeredPane1.add(btnUpdateProject, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 70, 130, 40));

        btnDeleteProject.setBackground(new java.awt.Color(255, 255, 255));
        btnDeleteProject.setFont(btnDeleteProject.getFont());
        btnDeleteProject.setForeground(new java.awt.Color(0, 0, 0));
        btnDeleteProject.setText("Delete Project");
        btnDeleteProject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteProjectActionPerformed(evt);
            }
        });
        jLayeredPane1.add(btnDeleteProject, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 70, 130, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLayeredPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewProjectActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnViewProjectActionPerformed
    {//GEN-HEADEREND:event_btnViewProjectActionPerformed
        int selectedProject = tblProjects.getSelectedRow(),
            projectID;
        
        if (selectedProject > -1)
        {
            projectID = (int) tblProjects.getValueAt(selectedProject, 0);
            this.dispose();
            new ViewProject(projectID, "Admin");
        }
    }//GEN-LAST:event_btnViewProjectActionPerformed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        if (cmn.logout(false) == 0) this.dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (cmn.logout(true) == 0) this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void btnCreateProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateProjectActionPerformed
        this.dispose();
        new CreateProject();
    }//GEN-LAST:event_btnCreateProjectActionPerformed

    private void tblProjectsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblProjectsMouseClicked
        int selectedRow = tblProjects.getSelectedRow(),
            selectedProjectID;
        
        if (selectedRow > -1)
        {
            selectedProjectID = (int) tblProjects.getValueAt(selectedRow, 0);
            lblSelectedProject.setText((String) tblProjects.getValueAt(selectedRow, 1));
            setTblProjectMembers(selectedProjectID);
            setProjectStatus(selectedProjectID);
        }
    }//GEN-LAST:event_tblProjectsMouseClicked

    private void btnViewDatabaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewDatabaseActionPerformed
        this.dispose();
        new ViewDatabase();
    }//GEN-LAST:event_btnViewDatabaseActionPerformed

    private void btnUpdateProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateProjectActionPerformed
        int selectedProject = tblProjects.getSelectedRow(),
            projectID;
        
        if (selectedProject > -1)
        {
            projectID = (int) tblProjects.getValueAt(selectedProject, 0);
            this.dispose();
            new UpdateProject(projectID);
        }
    }//GEN-LAST:event_btnUpdateProjectActionPerformed

    private void btnDeleteProjectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteProjectActionPerformed
        int selectedProject = tblProjects.getSelectedRow(),
            projectID;
        
        if (selectedProject > -1)
        {
            projectID = (int) tblProjects.getValueAt(selectedProject, 0);
            
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to delete this project ?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION)
            {
                cmn.delete(projectID, Common.PROJECTDELETE);
                setTblProjects();
                
                lblSelectedProject.setText("Selected Project Name");
                
                Object tblProjectMembersHeaders[] = 
                {
                    "Member",
                    "Last Seen"
                };

                dtmMembers = new DefaultTableModel(tblProjectMembersHeaders, 0)
                {
                    @Override
                    public boolean isCellEditable(int row, int column) { return false; }
                };
                
                tblProjectMembers.setModel(dtmMembers);
                
                pbProjectProgress.setString("0% Completed");
                pbProjectProgress.setValue(0);
                
                pbProjectStatus.setString("Status");
                pbProjectStatus.setValue(0);
                
            }
        }
    }//GEN-LAST:event_btnDeleteProjectActionPerformed

    public static void main(String args[])
    {
        new ManageProjectsHome().setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateProject;
    private javax.swing.JButton btnDeleteProject;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnUpdateProject;
    private javax.swing.JButton btnViewDatabase;
    private javax.swing.JButton btnViewProject;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblManageProjects;
    private javax.swing.JLabel lblSelectedProject;
    private javax.swing.JProgressBar pbProjectProgress;
    private javax.swing.JProgressBar pbProjectStatus;
    private javax.swing.JPanel pnlProjectView;
    private javax.swing.JTable tblProjectMembers;
    private javax.swing.JTable tblProjects;
    // End of variables declaration//GEN-END:variables
}
