package alprojectmanagementsystem;

/**
 * <h1>View Database Screen</h1>
 * <p>Admin will be able to view 
 * the most important tables from 
 * the database</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class ViewDatabase
{
    
    JFrame f;

    JLabel lblTitle;

    JTabbedPane tbForTables;

    JPanel pnlStaff,
           pnlSessions,
           pnlProjects,
           pnlProjectMembers,
           pnlTasks,
           pnlTaskAssignees;

    JScrollPane spForTblStaff,
                spForTblSessions,
                spForTblProjects,
                spForTblProjectMembers,
                spForTblTasks,
                spForTblTaskAssignees;

    DefaultTableModel dtmStaff,
                      dtmSessions,
                      dtmProjects,
                      dtmProjectMembers,
                      dtmTasks,
                      dtmTaskAssignees;

    JTable tblStaff,
           tblSessions,
           tblProjects,
           tblProjectMembers,
           tblTasks,
           tblTaskAssignees;

    JButton btnBack;

    Connection dbConnection = Common.getConnection();
    PreparedStatement ps = null;
    ResultSet rs = null;

    public ViewDatabase()
    {
        f = new JFrame("AL Project Management System");
        f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        f.setLayout(null);
        f.setSize(900, 580);
        f.setResizable(false);
        f.setLocationRelativeTo(null);
        f.getContentPane().setBackground(Color.WHITE);
        f.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                f.dispose();
                new ManageProjectsHome().setVisible(true);
            }
        });

        lblTitle = new JLabel("DATABASE");
        lblTitle.setFont(new Font(lblTitle.getFont().toString(), Font.BOLD, 40));
        lblTitle.setSize(lblTitle.getPreferredSize());

        tbForTables = new JTabbedPane();
        tbForTables.setSize(f.getWidth() - 20, f.getHeight() - 160);

        pnlStaff = new JPanel();
        pnlStaff.setLayout(new GridLayout());
        pnlStaff.setBackground(Color.white);

        setDTMStaff();
        
        tblStaff = new JTable(dtmStaff);
        tblStaff.setFillsViewportHeight(true);

        spForTblStaff = new JScrollPane(tblStaff);

        pnlStaff.add(spForTblStaff);

        pnlSessions = new JPanel();
        pnlSessions.setLayout(new GridLayout());
        pnlSessions.setBackground(Color.white);

        setDTMSessions();
        
        tblSessions = new JTable(dtmSessions);
        tblSessions.setFillsViewportHeight(true);

        spForTblSessions = new JScrollPane(tblSessions);

        pnlSessions.add(spForTblSessions);

        pnlProjects = new JPanel();
        pnlProjects.setLayout(new GridLayout());
        pnlProjects.setBackground(Color.white);

        setDTMProjects();
        
        tblProjects = new JTable(dtmProjects);
        tblProjects.setFillsViewportHeight(true);

        spForTblProjects = new JScrollPane(tblProjects);

        pnlProjects.add(spForTblProjects);
        pnlSessions.add(spForTblSessions);

        pnlProjectMembers = new JPanel();
        pnlProjectMembers.setLayout(new GridLayout());
        pnlProjectMembers.setBackground(Color.white);

        setDTMProjectMembers();
        
        tblProjectMembers = new JTable(dtmProjectMembers);
        tblProjectMembers.setFillsViewportHeight(true);

        spForTblProjectMembers = new JScrollPane(tblProjectMembers);

        pnlProjectMembers.add(spForTblProjectMembers);

        pnlTasks = new JPanel();
        pnlTasks.setLayout(new GridLayout());
        pnlTasks.setBackground(Color.white);

        setDTMTasks();
        
        tblTasks = new JTable(dtmTasks);
        tblTasks.setFillsViewportHeight(true);

        spForTblTasks = new JScrollPane(tblTasks);

        pnlTasks.add(spForTblTasks);

        pnlTaskAssignees = new JPanel();
        pnlTaskAssignees.setLayout(new GridLayout());
        pnlTaskAssignees.setBackground(Color.white);

        setDTMTaskAssignees();
        
        tblTaskAssignees = new JTable(dtmTaskAssignees);
        tblTaskAssignees.setFillsViewportHeight(true);

        spForTblTaskAssignees = new JScrollPane(tblTaskAssignees);

        pnlTaskAssignees.add(spForTblTaskAssignees);

        tbForTables.add("Staff", pnlStaff);
        tbForTables.add("Sessions", pnlSessions);
        tbForTables.add("Projects", pnlProjects);
        tbForTables.add("Project Members", pnlProjectMembers);
        tbForTables.add("Tasks", pnlTasks);
        tbForTables.add("Task Assignees", pnlTaskAssignees);

        btnBack = new JButton("← Back");
        btnBack.setSize(btnBack.getPreferredSize());
        btnBack.setForeground(Color.black);
        btnBack.setBackground(Color.white);
        btnBack.addMouseListener
        (
            new MouseAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent me)
                {
                    f.dispose();
                    new ManageProjectsHome().setVisible(true);
                }
            }
        );
        
        btnBack.setLocation(20, 20);
        lblTitle.setLocation((f.getWidth() - lblTitle.getWidth()) / 2, 40);
        tbForTables.setLocation(8, f.getHeight() - tbForTables.getHeight() - 40);

        f.getContentPane().add(btnBack);
        f.getContentPane().add(lblTitle);
        f.getContentPane().add(tbForTables);

        f.setVisible(true);
    }

    void setDTMStaff()
    {
        Object tblStaffHeaders[] = 
        {
            "ID",
            "First Name",
            "Last Name",
            "Contact Number",
            "Email"
        };

        dtmStaff = new DefaultTableModel(tblStaffHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT * FROM STAFF"
            );

            rs = ps.executeQuery();

            while (rs.next())
            {
                Object member[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5)
                };

                dtmStaff.addRow(member);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setDTMSessions()
    {
        Object tblSessionsHeaders[] = 
        {
            "Staff Member",
            "Login Time",
            "Logout Time"
        };

        dtmSessions = new DefaultTableModel(tblSessionsHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT se.*, s.FIRST_NAME, s.LAST_NAME " + 
                "FROM SESSIONS se, STAFF s " +
                "WHERE s.STAFF_ID = se.STAFF_MEMBER_ID " +
                "ORDER BY se.STAFF_MEMBER_ID ASC"
            );

            rs = ps.executeQuery();

            while (rs.next())
            {
                Object row[] = 
                {
                    rs.getObject(5) + " " + rs.getObject(6),
                    rs.getObject(2),
                    rs.getObject(3)
                };

                dtmSessions.addRow(row);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setDTMProjects()
    {
        Object tblProjectsHeaders[] = 
        {
            "No",
            "Name",
            "Description",
            "Start Date",
            "End Date",
            "Owner"
        };
        
        dtmProjects = new DefaultTableModel(tblProjectsHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT p.*, s.FIRST_NAME, s.LAST_NAME " +
                "FROM PROJECTS p, STAFF s " +
                "WHERE s.STAFF_ID = p.OWNER_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object project[] = 
                {
                    rs.getObject(1),
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getObject(4),
                    rs.getObject(5),
                    rs.getObject(7) + " " + rs.getObject(8)
                };

                dtmProjects.addRow(project);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setDTMProjectMembers()
    {
        Object tblProjectMembersHeaders[] = 
        {
            "Project",
            "Member"
        };

        dtmProjectMembers = new DefaultTableModel(tblProjectMembersHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT pm.*, s.FIRST_NAME, s.LAST_NAME, p.PROJECT_NAME " +
                "FROM PROJECT_MEMBERS pm, STAFF s, PROJECTS p " +
                "WHERE s.STAFF_ID = pm.STAFF_ID AND p.PROJECT_ID = pm.PROJECT_ID " +
                "ORDER BY pm.PROJECT_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object projectMember[] = 
                {
                    rs.getObject(5),
                    rs.getObject(3) + " " + rs.getObject(4)
                };

                dtmProjectMembers.addRow(projectMember);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setDTMTasks()
    {
        Object tblTasksHeaders[] = 
        {
            "Task Name",
            "Description",
            "Complete",
            "Creator",
            "Project Name"
        };

        dtmTasks = new DefaultTableModel(tblTasksHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };

        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT t.*, s.FIRST_NAME, s.LAST_NAME, p.PROJECT_NAME " + 
                "FROM TASKS t, STAFF s, PROJECTS p " +
                "WHERE s.STAFF_ID = t.TASK_CREATOR AND p.PROJECT_ID = t.PROJECT_ID " +
                "ORDER BY t.PROJECT_ID ASC"
            );

            rs = ps.executeQuery();

            while (rs.next())
            {
                Object task[] = 
                {
                    rs.getObject(2),
                    rs.getObject(3),
                    rs.getInt(4) == 1 ? "Yes" : "No",
                    rs.getObject(7) + " " + rs.getObject(8),
                    rs.getObject(9)
                };

                dtmTasks.addRow(task);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

    void setDTMTaskAssignees()
    {
        Object tblTaskAssigneesHeaders[] = 
        {
            "Task",
            "Assignee"
        };

        dtmTaskAssignees = new DefaultTableModel(tblTaskAssigneesHeaders, 0)
        {
            @Override
            public boolean isCellEditable(int row, int column) { return false; }
        };
        
        try
        {
            ps = dbConnection.prepareStatement
            (
                "SELECT ta.*, s.FIRST_NAME, s.LAST_NAME, t.TASK_NAME " +
                "FROM TASK_ASSIGNEES ta, STAFF s, TASKS t " +
                "WHERE s.STAFF_ID = ta.ASSIGNEE_ID AND t.TASK_ID = ta.TASK_ID " +
                "ORDER BY ta.TASK_ID"
            );
            
            rs = ps.executeQuery();
            
            while (rs.next())
            {
                Object taskAssignee[] = 
                {
                    rs.getObject(5),
                    rs.getObject(3) + " " + rs.getObject(4)
                };

                dtmTaskAssignees.addRow(taskAssignee);
            }

            rs.close();
            ps.close();
        }
        catch (SQLException se) { JOptionPane.showMessageDialog(null, se.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); }
    }

}
