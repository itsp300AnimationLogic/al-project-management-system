package alprojectmanagementsystem;

/**
 * <h1>Application Entry Point</h1>
 * <p>It contains the main method with calls
 * the login screen</p>
 * 
 * @author Shaylen Reddy [shaylenreddy42@gmail.com]
 * @version 0
 * @since ?
 */

public class ALProjectManagementSystem
{

    public static void main(String[] args)
    {
        new Login();
    }
    
}
